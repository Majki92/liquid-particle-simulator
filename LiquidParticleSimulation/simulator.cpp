#include "simulator.h"
#include <QDebug>

ParticleCollection s_particles;

Simulator::Simulator(QObject *parent)
   : QObject(parent),m_particles(&s_particles), m_simThread(m_particles)
{
    srand(time(NULL));
    m_particles->count = 0;
    qRegisterMetaType<SimImplType>("SimImplType");
    m_simThread.start();
    QObject::connect(this,SIGNAL(StopSig()),&m_simThread,SLOT(Stop()));
    QObject::connect(this,SIGNAL(StartSig()),&m_simThread,SLOT(Start()));
    QObject::connect(this,SIGNAL(TerminateSig()),&m_simThread,SLOT(Terminate()));
    QObject::connect(this,SIGNAL(ResetSig(SimImplType)),&m_simThread,SLOT(Reset(SimImplType)));
    QObject::connect(this,SIGNAL(SetSpeedSig(uint32_t)),&m_simThread,SLOT(SetSpeed(uint32_t)));
}
