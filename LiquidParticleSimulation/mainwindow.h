#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <cstdint>

#include "simulator.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    Simulator m_simulator;
    QTimer *timer;

private slots:
    void OnSpeedSliderMove(int position);
    void OnStartBtnClk();
    void OnResetBtnClk();
    void onCppRadioToggled();
    void Update();
    void on_radioOpenCL_toggled(bool checked);
    void on_radioCUDA_toggled(bool checked);
};

#endif // MAINWINDOW_H
