#ifndef UTILS_H
#define UTILS_H

#define GLM_FORCE_RADIANS

#include <glm.hpp>
#include <gtc/constants.hpp>
#include <gtc/matrix_transform.hpp>
#include <gtc/matrix_inverse.hpp>
#include <gtc/type_ptr.hpp>
#include <gtx/matrix_interpolation.hpp>
#include <cstring>
#include <iostream>
#include <vector>
#include <QOpenGLFunctions_4_1_Core>

/* All secondary things such as vertex layout, material layout and light */
class PointLight
{
public:
    PointLight() { memset(this, sizeof(this), 0); }

	glm::vec4 mAmbient;
	glm::vec4 mDiffuse;
	glm::vec4 mSpecular;
	glm::vec4 mPosition;
	glm::mat4 mViewProj;

};

class planeVertex
{
public:
    planeVertex() { memset(this, sizeof(this), 0); }
    planeVertex(glm::vec4 pos, glm::vec2 tex) : mPos(pos), mTex(tex) {}
    planeVertex(float posx, float posy, float posz, float texx, float texy)
    {
        mPos = glm::vec4(posx, posy, posz, 1.0f);
        mTex = glm::vec2(texx, texy);
    }
    glm::vec4 mPos;
    glm::vec2 mTex;
};

class GLContext
{
public:
    static QOpenGLFunctions_4_1_Core* gl;
};

void CheckOpenGLError(const char* stmt, const char* fname, int line);

#define GL_CHECK(stmt) do { \
           GLContext::gl->stmt; \
           CheckOpenGLError(#stmt, __FILE__, __LINE__); \
       } while (0)



#endif
