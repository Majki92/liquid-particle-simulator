#ifndef GEOMETRYGENERATOR_H
#define GEOMETRYGENERATOR_H

#include "utils.h"
#include <vector>

class GeometryGenerator
{
public:
    static void createSphereSky(float radius, unsigned int sliceCount, unsigned int stackCount, std::vector<glm::vec4>* vertices, std::vector<GLuint>* indices);
    static void createPlane(glm::vec2 size, std::vector<planeVertex>* vertices, std::vector<GLuint>* indices);
};

#endif // GEOMETRYGENERATOR_H
