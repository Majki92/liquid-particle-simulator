#ifndef RENDERER_H
#define RENDERER_H

#include <QOpenGLWidget>
#include <QElapsedTimer>
#include "shader.h"
#include "renderobject.h"
#include "camera.h"
#include <QPushButton>
#include <QOpenGLFunctions_4_1_Core>

// Good resource for implementation
// http://http.developer.nvidia.com/GPUGems3/gpugems3_ch30.html

class OpenGLError;
class QMoveEvent;

struct ParticleCollection;

struct VertexQuad
{
    glm::vec3 pt;
    glm::vec2 texcoord;
};

struct DeviceMesh
{
    unsigned int vertex_array;
    unsigned int vbo_indices;
    unsigned int num_indices;
    unsigned int vbo_data;
};

class Renderer : public QOpenGLWidget, protected QOpenGLFunctions_4_1_Core
{
    Q_OBJECT
public:
    static const uint64_t FPSFrameDelay = 60;
    explicit Renderer(QWidget *parent = 0);

    void SetCollection(ParticleCollection* collection) { m_particles = collection; }
    void SetStartButton(QPushButton* button) { m_button = button; }
    void SwitchSimulator(bool cpu);
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();
    void initFBO(int w, int h);
    void initQuad();
private:
    ParticleCollection* m_particles;
    QPushButton* m_button;
    Sky* m_sky;
    Particle* m_renderParticles;
    Plane* m_plane;
    Camera m_camera;
    bool m_fpsMode;
    std::map<std::string, Shader*> m_shaders;
    int m_height;
    int m_width;
    bool event(QEvent *event);
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseDoubleClickEvent(QMouseEvent *event);
    void printVersionInformation();

    GLuint m_depthTexture;
    GLuint m_positionTexture;
    GLuint m_normalTexture;
    GLuint m_backgroundTexture;
    GLuint m_blurDepthTexture;
    GLuint m_thickTexture;

    GLuint m_FBO;
    GLuint m_normalsFBO;
    GLuint m_backgroundFBO;
    GLuint m_blurDepthFBO;
    GLuint m_thickFBO;

    DeviceMesh deviceQuad;
signals:

public slots:
    void updateFrame();
};

#endif // RENDERER_H
