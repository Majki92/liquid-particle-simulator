#ifndef RENDEROBJECT_H
#define RENDEROBJECT_H

#include "utils.h"

struct ParticleCollection;

class RenderObject
{
public:
    RenderObject();
    virtual ~RenderObject(){}
    GLuint mVB;
    GLuint mIB;
    GLuint mVAO;
};

/* Class representing skybox (skysphere) */
class Sky : public RenderObject
{
public:
    Sky(const std::string& cubemapFilename, float skySphereRadius);
    GLuint mTexture;
    unsigned int mCount;
};

class Plane : public RenderObject
{
public:
    Plane(const std::string& mapFilename, int sizeX, int sizeY);
    GLuint mTexture;
    unsigned int mCount;
};

class Particle : public RenderObject
{
public:
    Particle(ParticleCollection* particles);
    unsigned int mCount;
};

#endif
