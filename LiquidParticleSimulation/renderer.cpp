#include "renderer.h"
#include <QDebug>
#include <QKeyEvent>
#include <simulator.h>
#include "utils.h"

#define BUFFER_OFFSET(i) ((char *)NULL + (i))

Renderer::Renderer(QWidget *parent)
: QOpenGLWidget(parent), m_particles(NULL), m_shaders()
{
    setMouseTracking(true);
    m_fpsMode = false;
    grabKeyboard();
    qDebug() << "I'm alive";
}

void Renderer::initializeGL() {
    initializeOpenGLFunctions();
    GLContext::gl = new QOpenGLFunctions_4_1_Core();
    GLContext::gl->initializeOpenGLFunctions();
    connect(this, SIGNAL(frameSwapped()), this, SLOT(updateFrame()));
    printVersionInformation();
    GL_CHECK(glClearColor(1.0f, 1.0f, 1.0f, 1.0f));
    GL_CHECK(glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS));

    m_particles->m_gpu = false;
    GL_CHECK(glGenBuffers(1, &(m_particles->m_vbCPU)));
    GL_CHECK(glGenVertexArrays(1, &(m_particles->m_vaoCPU)));
    GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, m_particles->m_vbCPU));
    GL_CHECK(glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * m_particles->MAX_PARTICLES, 0, GL_DYNAMIC_DRAW));
    GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, 0));

    GL_CHECK(glGenBuffers(1, &(m_particles->m_vbGPU)));
    GL_CHECK(glGenVertexArrays(1, &(m_particles->m_vaoGPU)));
    GL_CHECK(glBindVertexArray(m_particles->m_vaoGPU));
    GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, m_particles->m_vbGPU));
    GL_CHECK(glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * m_particles->MAX_PARTICLES, 0, GL_DYNAMIC_DRAW));
    GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, 0));
    GL_CHECK(glBindVertexArray(0));

    m_shaders["sky"] = new SkyShader("media/shaders/SkyShader");
    m_shaders["test"] = new TestShader("media/shaders/TestShader");
    m_shaders["particle"] = new ParticleShader("media/shaders/ParticleShader");
    m_shaders["thick"] = new ThickShader("media/shaders/ThickShader");
    m_shaders["blur"] = new BlurShader("media/shaders/BlurShader");
    m_shaders["normal"] = new NormalShader("media/shaders/NormalShader");
    m_shaders["resolve"] = new ResolveShader("media/shaders/ResolveShader");

    m_sky = new Sky("media/textures/TropicalSunny.png", 400.0f);
    m_renderParticles = new Particle(m_particles);
    m_plane = new Plane("media/textures/marble.jpg", 500, 500);
    m_button->setEnabled(true);

    initQuad();
}

void Renderer::resizeGL(int w, int h) {
   GL_CHECK(glViewport(0, 0, w, h));
   m_width = w;
   m_height = h;
   initFBO(m_width, m_height);
}

void Renderer::paintGL()
{

    GLuint prevFBO = 0;
    /* Enum has MANY names based on extension/version but they all map to 0x8CA6 */
    GL_CHECK(glGetIntegerv(0x8CA6, (GLint*)&prevFBO));
    //Render Skybox To Color Texture
    GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, m_backgroundFBO));
    //GL_CHECK(glClearColor(1, 1, 1, 1));
    //GL_CHECK(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));

    //SKY

    GL_CHECK(glUseProgram(m_shaders["sky"]->mProgram));

    // Translate sky as camera to get proper distance, sky is standing still
    glm::mat4 T = glm::translate(glm::mat4(1.0), m_camera.GetPosition());
    glm::mat4 MVP = m_camera.ViewProj() * T;

    m_shaders["sky"]->SetWorldViewProj(MVP);
    m_shaders["sky"]->SetCubeMap(m_sky->mTexture);

    // Set OpenGL properties
    GL_CHECK(glDisable(GL_CULL_FACE));
    GL_CHECK(glEnable(GL_DEPTH_TEST));
    GL_CHECK(glDepthFunc(GL_LEQUAL));

    // bind proper buffers
    GL_CHECK(glBindVertexArray(m_sky->mVAO));
    GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, m_sky->mVB));
    GL_CHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_sky->mIB));

    // Set layout
    GL_CHECK(glEnableVertexAttribArray(0));

    GL_CHECK(glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0)));

    GL_CHECK(glDrawElements(GL_TRIANGLES, m_sky->mCount, GL_UNSIGNED_INT, 0));

    // Clean up OpenGL state
    GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, 0));
    GL_CHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0));
    GL_CHECK(glDisableVertexAttribArray(0));
    m_shaders["sky"]->SetCubeMap(0);
    GL_CHECK(glEnable(GL_CULL_FACE));
    GL_CHECK(glDisable(GL_DEPTH_TEST));
    GL_CHECK(glBindVertexArray(0));

    //PLANE
    GL_CHECK(glUseProgram(m_shaders["test"]->mProgram));
    m_shaders["test"]->SetWorldViewProj(m_camera.ViewProj());
    m_shaders["test"]->SetTestMap(m_plane->mTexture);
    GL_CHECK(glDisable(GL_CULL_FACE));
    GL_CHECK(glEnable(GL_DEPTH_TEST));
    GL_CHECK(glBindVertexArray(m_plane->mVAO));
    GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, m_plane->mVB));
    GL_CHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_plane->mIB));
    GL_CHECK(glEnableVertexAttribArray(0));
    GL_CHECK(glEnableVertexAttribArray(1));
    GL_CHECK(glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(planeVertex), BUFFER_OFFSET(0)));
    GL_CHECK(glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(planeVertex), BUFFER_OFFSET(sizeof(glm::vec4))));
    GL_CHECK(glDrawElements(GL_TRIANGLES, m_plane->mCount, GL_UNSIGNED_INT, 0));
    GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, 0));
    GL_CHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0));
    GL_CHECK(glDisableVertexAttribArray(0));
    GL_CHECK(glDisableVertexAttribArray(1));

    GL_CHECK(glEnable(GL_CULL_FACE));
    GL_CHECK(glDisable(GL_DEPTH_TEST));
    GL_CHECK(glBindVertexArray(0));

    int particleCount = m_particles->count;

    // PARTICLES depth
    GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, m_FBO));
    glClearColor(1, 1, 1, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    GL_CHECK(glUseProgram(m_shaders["particle"]->mProgram));
    GL_CHECK(glEnable(GL_PROGRAM_POINT_SIZE));
    m_shaders["particle"]->SetView(m_camera.View());
    m_shaders["particle"]->SetProj(m_camera.Proj());
    m_shaders["particle"]->SetPointRadius(0.125f);
    m_shaders["particle"]->SetPointScale(576.0f / glm::tan(45.0f * 0.5f * 3.14f / 180.0f));

    // Set OpenGL properties
    GL_CHECK(glDisable(GL_CULL_FACE));
    GL_CHECK(glEnable(GL_DEPTH_TEST));
    if(m_particles->m_gpu)
    {
       // bind proper buffers
       GL_CHECK(glBindVertexArray(m_particles->m_vaoGPU));
       GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, m_particles->m_vbGPU));
    }
    else
    {
        // bind proper buffers
        GL_CHECK(glBindVertexArray(m_particles->m_vaoCPU));
        GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, m_particles->m_vbCPU));
        GL_CHECK(glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(glm::vec3) * particleCount, &(m_particles->pos[0])));
        GL_CHECK(glFinish());
    }

   // Set layout
   GL_CHECK(glEnableVertexAttribArray(0));

   GL_CHECK(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0)));

   GL_CHECK(glDrawArrays(GL_POINTS, 0, particleCount));

   // Clean up OpenGL state
   GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, 0));
   GL_CHECK(glDisableVertexAttribArray(0));
   GL_CHECK(glEnable(GL_CULL_FACE));
   GL_CHECK(glDisable(GL_DEPTH_TEST));
   GL_CHECK(glBindVertexArray(0));

    // PARTICLES thick
    GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, m_thickFBO));
    glClearColor(0, 0, 0, 0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    GL_CHECK(glEnable(GL_PROGRAM_POINT_SIZE));

    glEnable(GL_BLEND);
    glDisable(GL_DEPTH_TEST);
    glBlendFunc(GL_ONE,GL_ONE);

    GL_CHECK(glUseProgram(m_shaders["thick"]->mProgram));
    GL_CHECK(glEnable(GL_PROGRAM_POINT_SIZE));
    m_shaders["thick"]->SetView(m_camera.View());
    m_shaders["thick"]->SetProj(m_camera.Proj());
    m_shaders["thick"]->SetPointRadius(0.125f);
    m_shaders["thick"]->SetPointScale(576.0f / glm::tan(45.0f * 0.5f * 3.14f / 180.0f));

    // Set OpenGL properties
    GL_CHECK(glDisable(GL_CULL_FACE));
    GL_CHECK(glEnable(GL_DEPTH_TEST));
    if(m_particles->m_gpu)
    {
       // bind proper buffers
       GL_CHECK(glBindVertexArray(m_particles->m_vaoGPU));
       GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, m_particles->m_vbGPU));
    }
    else
    {
        // bind proper buffers
        GL_CHECK(glBindVertexArray(m_particles->m_vaoCPU));
        GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, m_particles->m_vbCPU));
    }

    // Set layout
    GL_CHECK(glEnableVertexAttribArray(0));

    GL_CHECK(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0)));

    GL_CHECK(glDrawArrays(GL_POINTS, 0, particleCount));

    // Clean up OpenGL state
    GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, 0));
    GL_CHECK(glDisableVertexAttribArray(0));
    GL_CHECK(glEnable(GL_CULL_FACE));
    GL_CHECK(glDisable(GL_DEPTH_TEST));
    GL_CHECK(glBindVertexArray(0));

    glDisable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);

    //PARTICLES blur

    GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, m_blurDepthFBO));

    GL_CHECK(glUseProgram(m_shaders["blur"]->mProgram));
    glBindVertexArray(deviceQuad.vertex_array);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, deviceQuad.vbo_indices);

    glActiveTexture(GL_TEXTURE11);
    glBindTexture(GL_TEXTURE_2D, m_depthTexture);

    GL_CHECK(glEnable(GL_PROGRAM_POINT_SIZE));
    m_shaders["blur"]->SetDepthTex(11);
    m_shaders["blur"]->SetFar(400.0f);
    m_shaders["blur"]->SetNear(1.0f);
    GL_CHECK(glFinish());
    GL_CHECK(glEnableVertexAttribArray(0));
    GL_CHECK(glEnableVertexAttribArray(1));
    glDrawElements(GL_TRIANGLES, deviceQuad.num_indices, GL_UNSIGNED_SHORT,0);
    GL_CHECK(glDisableVertexAttribArray(0));
    GL_CHECK(glDisableVertexAttribArray(1));
    glBindVertexArray(0);

    //PARTICLES normal
     GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, m_normalsFBO));

    GL_CHECK(glUseProgram(m_shaders["normal"]->mProgram));
    glBindVertexArray(deviceQuad.vertex_array);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, deviceQuad.vbo_indices);


    glActiveTexture(GL_TEXTURE12);
    glBindTexture(GL_TEXTURE_2D, m_blurDepthTexture);
    m_shaders["normal"]->SetDepthTex(12);

    m_shaders["normal"]->SetFar(400.0f);
    m_shaders["normal"]->SetNear(1.0f);

    m_shaders["normal"]->SetInvProj(m_camera.InvProj());
    GL_CHECK(glFinish());
    GL_CHECK(glEnableVertexAttribArray(0));
    GL_CHECK(glEnableVertexAttribArray(1));
    glDrawElements(GL_TRIANGLES, deviceQuad.num_indices, GL_UNSIGNED_SHORT,0);
    GL_CHECK(glDisableVertexAttribArray(0));
    GL_CHECK(glDisableVertexAttribArray(1));
    glBindVertexArray(0);

    // PARTICLES resolve
    GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, prevFBO));
    GL_CHECK(glUseProgram(m_shaders["resolve"]->mProgram));
    glBindVertexArray(deviceQuad.vertex_array);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, deviceQuad.vbo_indices);

    glActiveTexture(GL_TEXTURE12);
    glBindTexture(GL_TEXTURE_2D, m_blurDepthTexture);
    m_shaders["resolve"]->SetDepthTex(12);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, m_normalTexture);
    m_shaders["resolve"]->SetNormalTex(1);
    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, m_positionTexture);
    m_shaders["resolve"]->SetPosTex(3);
    glActiveTexture(GL_TEXTURE4);
    glBindTexture(GL_TEXTURE_2D, m_backgroundTexture);
    m_shaders["resolve"]->SetBackgroundTex(4);
    m_shaders["resolve"]->SetCubeMap(m_sky->mTexture);
    glActiveTexture(GL_TEXTURE7);
    glBindTexture(GL_TEXTURE_2D, m_thickTexture);
    m_shaders["resolve"]->SetThickTex(7);
    m_shaders["resolve"]->SetView(m_camera.View());
    m_shaders["resolve"]->SetInvProj(m_camera.InvProj());
    m_shaders["resolve"]->SetFar(400.0f);
    m_shaders["resolve"]->SetNear(1.0f);
    GL_CHECK(glFinish());
    GL_CHECK(glEnableVertexAttribArray(0));
    GL_CHECK(glEnableVertexAttribArray(1));
    glDrawElements(GL_TRIANGLES, deviceQuad.num_indices, GL_UNSIGNED_SHORT, 0);
    GL_CHECK(glDisableVertexAttribArray(0));
    GL_CHECK(glDisableVertexAttribArray(1));
    glBindVertexArray(0);
}

void Renderer::updateFrame()
{
  m_camera.UpdateViewMatrix();
  // Schedule a redraw
  update();
}

void Renderer::SwitchSimulator(bool cpu)
{

}

void Renderer::printVersionInformation()
{
  QString glType;
  QString glVersion;
  QString glProfile;

  // Get Version Information
  glType = (context()->isOpenGLES()) ? "OpenGL ES" : "OpenGL";
  glVersion = reinterpret_cast<const char*>(glGetString(GL_VERSION));

  // Get Profile Information
#define CASE(c) case QSurfaceFormat::c: glProfile = #c; break
  switch (format().profile())
  {
    CASE(NoProfile);
    CASE(CoreProfile);
    CASE(CompatibilityProfile);
  }
#undef CASE

  // qPrintable() will print our QString w/o quotes around it.
  qDebug() << qPrintable(glType) << qPrintable(glVersion) << "(" << qPrintable(glProfile) << ")";
}

bool Renderer::event(QEvent *e)
{
  return QOpenGLWidget::event(e);
}

void Renderer::keyPressEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Up)
      m_camera.Pitch(0.05f);
    if(event->key() == Qt::Key_Down)
      m_camera.Pitch(-0.05f);
    if(event->key() == Qt::Key_Left)
      m_camera.RotateY(0.05f);
    if(event->key() == Qt::Key_Right)
      m_camera.RotateY(-0.05f);
    if(event->key() == Qt::Key_W)
      m_camera.Walk(1.5f);
    if(event->key() == Qt::Key_S)
      m_camera.Walk(-1.5f);
    if(event->key() == Qt::Key_A)
      m_camera.Strafe(-1.5f);
    if(event->key() == Qt::Key_D)
      m_camera.Strafe(1.5f);
    if(event->key() == Qt::Key_Escape)
    {
         m_fpsMode = false;
         setCursor(Qt::ArrowCursor);
    }
    //qDebug() << "KEY PRESSED: " << event->key();
}

void Renderer::keyReleaseEvent(QKeyEvent *event)
{
  if (event->isAutoRepeat())
  {
    event->ignore();
  }
  else
  {
   //qDebug() << "KEY RELEASED: " << event->key();
  }
}

void Renderer::mousePressEvent(QMouseEvent *event)
{
 // qDebug() << "MOUSE BUTTON PRESSED: " << event->button();
}

void Renderer::mouseReleaseEvent(QMouseEvent *event)
{
 // qDebug() << "MOUSE BUTTON RELEASED: " << event->button();
}

void Renderer::mouseDoubleClickEvent(QMouseEvent *event)
{
    if(event->buttons() & Qt::LeftButton)
    {
        m_fpsMode = true;
        cursor().setPos(mapToGlobal(QPoint(width() / 2, height() / 2)));
        setCursor(Qt::BlankCursor);
    }
}

void Renderer::mouseMoveEvent(QMouseEvent *event)
{
    static int x = width() / 2;
    static int y = height() / 2;
    if(m_fpsMode)
    {
        if(event->x() != x || event->y() != y)
        {
            float dx = (glm::radians<float>(0.25f*static_cast<float>(event->x() - x)));
            float dy = (glm::radians<float>(0.25f*static_cast<float>(event->y() - y)));
            m_camera.Pitch(dy);
            m_camera.RotateY(dx);
        }
        cursor().setPos(mapToGlobal(QPoint(x, y)));
    }
}

void Renderer::initFBO(int w, int h)
{
    GLenum FBOstatus;
    GL_CHECK(glActiveTexture(GL_TEXTURE10));
    GL_CHECK(glGenTextures(1, &m_depthTexture));
    GL_CHECK(glGenTextures(1, &m_normalTexture));
    GL_CHECK(glGenTextures(1, &m_positionTexture));
    GL_CHECK(glGenTextures(1, &m_backgroundTexture));
    GL_CHECK(glGenTextures(1, &m_blurDepthTexture));
    GL_CHECK(glGenTextures(1, &m_thickTexture));

    //Depth Texture Initiaialization
    GL_CHECK(glBindTexture(GL_TEXTURE_2D, m_depthTexture));

    GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST));
    GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST));

    GL_CHECK(glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP));
    GL_CHECK(glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP));

    GL_CHECK(glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, w, h, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0));

    //Blurred Depth Texture
    GL_CHECK(glBindTexture(GL_TEXTURE_2D, m_blurDepthTexture));

    GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST));
    GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST));

    GL_CHECK(glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP));
    GL_CHECK(glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP));

    GL_CHECK(glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB32F, w, h, 0, GL_RGBA, GL_FLOAT, 0));

    //Blurred Thick  Texture
    GL_CHECK(glBindTexture(GL_TEXTURE_2D, m_thickTexture));

    GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST));
    GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST));

    GL_CHECK(glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP));
    GL_CHECK(glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP));

    GL_CHECK(glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB32F, w, h, 0, GL_RGBA, GL_FLOAT, 0));

    //Normal Texture Initialization
    GL_CHECK(glBindTexture(GL_TEXTURE_2D, m_normalTexture));

    GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
    GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));

    GL_CHECK(glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP));
    GL_CHECK(glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP));

    GL_CHECK(glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB32F , w, h, 0, GL_RGBA, GL_FLOAT,0));

    //Position Texture Initialization
    GL_CHECK(glBindTexture(GL_TEXTURE_2D, m_positionTexture));

    GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
    GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));

    GL_CHECK(glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP));
    GL_CHECK(glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP));

    GL_CHECK(glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB32F , w, h, 0, GL_RGBA, GL_FLOAT,0));

    //Background Texture Initialization
    GL_CHECK(glBindTexture(GL_TEXTURE_2D, m_backgroundTexture));

    GL_CHECK(glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
    GL_CHECK(glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));

    GL_CHECK(glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP));
    GL_CHECK(glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP));

    GL_CHECK(glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB , w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, 0));

    GL_CHECK(glBindTexture(GL_TEXTURE_2D, 0));

    GL_CHECK(glGenFramebuffers(1, &m_FBO));
    GL_CHECK(glGenFramebuffers(1, &m_normalsFBO));
    GL_CHECK(glGenFramebuffers(1, &m_backgroundFBO));
    GL_CHECK(glGenFramebuffers(1, &m_blurDepthFBO));
    GL_CHECK(glGenFramebuffers(1, &m_thickFBO));

    GLuint prevFBO = 0;
    /* Enum has MANY names based on extension/version but they all map to 0x8CA6 */
    GL_CHECK(glGetIntegerv(0x8CA6, (GLint*)&prevFBO));

    //Create First Framebuffer Object
    GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, m_FBO));

    // Instruct openGL that we won't bind a color texture with the currently binded FBO
    GL_CHECK(glReadBuffer(GL_NONE));
    GL_CHECK(glDrawBuffer(GL_COLOR_ATTACHMENT0));
    GL_CHECK(glBindTexture(GL_TEXTURE_2D, m_depthTexture));
    GL_CHECK(glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, m_depthTexture, 0));
    GL_CHECK(glBindTexture(GL_TEXTURE_2D, m_positionTexture));
    GL_CHECK(glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, m_positionTexture, 0));

    // check FBO status
    FBOstatus = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if(FBOstatus != GL_FRAMEBUFFER_COMPLETE)
    {
        qDebug() << FBOstatus;
    }

    //Create Normals FBO (FBO to store Normals Data)
    GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, m_normalsFBO));
    GL_CHECK(glReadBuffer(GL_NONE));
    GL_CHECK(glDrawBuffer(GL_COLOR_ATTACHMENT0));
    GL_CHECK(glBindTexture(GL_TEXTURE_2D, m_normalTexture));
    GL_CHECK(glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, m_normalTexture, 0));

    // check FBO status
    FBOstatus = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if(FBOstatus != GL_FRAMEBUFFER_COMPLETE)
    {
        qDebug() << FBOstatus;
    }

    //Create Background FBO (FBO to store Background Data)
    GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, m_backgroundFBO));
    GL_CHECK(glReadBuffer(GL_NONE));
    GL_CHECK(glDrawBuffer(GL_COLOR_ATTACHMENT0));
    GL_CHECK(glBindTexture(GL_TEXTURE_2D, m_backgroundTexture));
    GL_CHECK(glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, m_backgroundTexture, 0));

    // check FBO status
    FBOstatus = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if(FBOstatus != GL_FRAMEBUFFER_COMPLETE)
    {
        qDebug() << FBOstatus;
    }

    //Create Blurred Depth FBO (FBO to store Blurred Depth Data)
    GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, m_blurDepthFBO));
    GL_CHECK(glReadBuffer(GL_NONE));
    GL_CHECK(glDrawBuffer(GL_COLOR_ATTACHMENT0));
    GL_CHECK(glBindTexture(GL_TEXTURE_2D, m_blurDepthTexture));
    GL_CHECK(glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, m_blurDepthTexture, 0));

    // check FBO status
    FBOstatus = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if(FBOstatus != GL_FRAMEBUFFER_COMPLETE)
    {
        qDebug() << FBOstatus;
    }

    //Create Thickness FBO (FBO to store Thickness Data)
    GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, m_thickFBO));
    GL_CHECK(glReadBuffer(GL_NONE));
    GL_CHECK(glDrawBuffer(GL_COLOR_ATTACHMENT0));
    GL_CHECK(glBindTexture(GL_TEXTURE_2D, m_thickTexture));
    GL_CHECK(glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, m_thickTexture, 0));

    // check FBO status
    FBOstatus = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if(FBOstatus != GL_FRAMEBUFFER_COMPLETE)
    {
        qDebug() << FBOstatus;
    }

    GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, prevFBO));
}

void Renderer::initQuad() {
    VertexQuad verts [] =
    {{glm::vec3(-1,1,0), glm::vec2(0,1)},
    {glm::vec3(-1,-1,0), glm::vec2(0,0)},
    {glm::vec3(1,-1,0), glm::vec2(1,0)},
    {glm::vec3(1,1,0), glm::vec2(1,1)}};

    unsigned short indices[] = { 0,1,2,0,2,3};

    //Allocate vertex array
    //Vertex arrays encapsulate a set of generic vertex attributes and the buffers they are bound too
    //Different vertex array per mesh.
    glGenVertexArrays(1, &(deviceQuad.vertex_array));
    glBindVertexArray(deviceQuad.vertex_array);


    //Allocate vbos for data
    glGenBuffers(1,&(deviceQuad.vbo_data));
    glGenBuffers(1,&(deviceQuad.vbo_indices));

    //Upload vertex data
    glBindBuffer(GL_ARRAY_BUFFER, deviceQuad.vbo_data);
    glBufferData(GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STATIC_DRAW);
    //Use of strided data, Array of Structures instead of Structures of Arrays
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexQuad), 0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(VertexQuad), (void*)sizeof(glm::vec3));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    //indices
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, deviceQuad.vbo_indices);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6*sizeof(GLushort), indices, GL_STATIC_DRAW);
    deviceQuad.num_indices = 6;
    //Unplug Vertex Array
    glBindVertexArray(0);
    }
