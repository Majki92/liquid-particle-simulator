#include "geometrygenerator.h"

void GeometryGenerator::createSphereSky(float radius, unsigned int sliceCount, unsigned int stackCount, std::vector<glm::vec4>* vertices, std::vector<GLuint>* indices)
{
    if (vertices == NULL || indices == NULL)
        return;

    vertices->clear();
    indices->clear();


    /* Compute the vertices stating at the top pole and moving down the stacks */

    glm::vec4 topVertex(0.0f, radius, 0.0f, 1.0f);
    glm::vec4 bottomVertex(0.0f, -radius, 0.0f, 1.0f);

    vertices->push_back(topVertex);

    float phiStep = glm::pi<float>() / stackCount;
    float thetaStep = 2.0f *  glm::pi<float>() / sliceCount;

    /* Compute vertices for each stack ring */
    for (unsigned int i = 1; i <= stackCount - 1; ++i)
    {
        float phi = i * phiStep;

        /* Vertices of ring */
        for (unsigned int j = 0; j <= sliceCount; ++j)
        {
            float theta = j * thetaStep;

            glm::vec4 v;

            /* Spherical to cartesian */
            v.x = radius*sinf(phi)*cosf(theta);
            v.y = radius*cosf(phi);
            v.z = radius*sinf(phi)*sinf(theta);
            v.w = 1.0f;

            vertices->push_back(v);
        }
    }

    vertices->push_back(bottomVertex);

    /* Compute indices for top stack */
    for (unsigned int i = 1; i <= sliceCount; ++i)
    {
        indices->push_back(0);
        indices->push_back(i + 1);
        indices->push_back(i);
    }

    /* Compute indices for inner stacks (not connected to poles) */
    unsigned int baseIndex = 1;
    unsigned int ringVertexCount = sliceCount + 1;
    for (unsigned int i = 0; i < stackCount - 2; ++i)
    {
        for (unsigned int j = 0; j < sliceCount; ++j)
        {
            indices->push_back(baseIndex + i*ringVertexCount + j);
            indices->push_back(baseIndex + i*ringVertexCount + j + 1);
            indices->push_back(baseIndex + (i + 1)*ringVertexCount + j);

            indices->push_back(baseIndex + (i + 1)*ringVertexCount + j);
            indices->push_back(baseIndex + i*ringVertexCount + j + 1);
            indices->push_back(baseIndex + (i + 1)*ringVertexCount + j + 1);
        }
    }


    /* Compute indices for bottom stack */
    unsigned int southPoleIndex = (unsigned int)vertices->size() - 1;
    baseIndex = southPoleIndex - ringVertexCount;

    for (unsigned int i = 0; i < sliceCount; ++i)
    {
        indices->push_back(southPoleIndex);
        indices->push_back(baseIndex + i);
        indices->push_back(baseIndex + i + 1);
    }
}

void  GeometryGenerator::createPlane(glm::vec2 size, std::vector<planeVertex>* vertices, std::vector<GLuint>* indices)
{
    if (vertices == NULL || indices == NULL)
        return;

    vertices->clear();
    indices->clear();

    /* Tessellation factors for plane */
    unsigned int m = 20;
    unsigned int n = 20;
    unsigned int vertexCount = m * n;
    unsigned int faceCount = (m - 1) * (n - 1) * 2;

    float halfWidth = 0.5f * size.x;
    float halfDepth = 0.5f * size.y;

    float dx = size.x / (n - 1);
    float dz = size.y / (m - 1);

    float du = 1.0f / (n - 1);
    float dv = 1.0f / (m - 1);

    planeVertex vert;
    for (unsigned int i = 0; i < m; ++i)
    {
        float z = halfDepth - i * dz;
        for (unsigned int j = 0; j < n; ++j)
        {
            float x = -halfWidth + j*dx;

            vert.mPos = glm::vec4(x, 0.0f, z, 1.0f);
            vert.mTex = glm::vec2(j * du, i * dv);
            vertices->push_back(vert);
        }
    }

    /* Iterate over each quad and compute indices */
    unsigned int k = 0;
    for (unsigned int i = 0; i < m - 1; ++i)
    {
        for (unsigned int j = 0; j < n - 1; ++j)
        {
            indices->push_back(i * n + j);
            indices->push_back(i * n + j + 1);
            indices->push_back((i + 1)*n + j);

            indices->push_back((i + 1) * n + j);
            indices->push_back(i * n + j + 1);
            indices->push_back((i + 1) * n + j + 1);

            k += 6;
        }
    }
}
