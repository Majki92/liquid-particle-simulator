#include "utils.h"
QOpenGLFunctions_4_1_Core* GLContext::gl = NULL;

void CheckOpenGLError(const char* stmt, const char* fname, int line)
{
    GLenum err = GLContext::gl->glGetError();
    if (err != GL_NO_ERROR)
    {
        printf("OpenGL error %08x, at %s:%i - for %s\n", err, fname, line, stmt);
    }
}
