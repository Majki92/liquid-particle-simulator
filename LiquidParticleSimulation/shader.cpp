#include "shader.h"
#include <QDebug>
#include <QFile>
using namespace std;



/* Loading shaders from file with .vert and .frag */
Shader::Shader(const std::string& filename)
{
	/* Create OpenGL program */
    mProgram = GLContext::gl->glCreateProgram();
    GLuint vert = GLContext::gl->glCreateShader(GL_VERTEX_SHADER);
    GLuint frag = GLContext::gl->glCreateShader(GL_FRAGMENT_SHADER);
	/* 0 - vertex 1 - fragment */
    for (int i = 0; i < 2; ++i)
    {
        string source(filename);
        if (i == 0)
            source.append(".vert");
        else
            source.append(".frag");
        QFile f(QString::fromStdString(source));
        if (!f.open(QFile::ReadOnly | QFile::Text))
            qDebug() << "Couldn't open shader file: " << source.c_str();
        QTextStream in(&f);
        string shader = in.readAll().toStdString();

		const GLchar *ss = (const GLchar *)shader.c_str();
        GL_CHECK(glShaderSource((i == 0) ? vert : frag, 1, &ss, NULL));
	}
	/* Compile, attach and link */
    GL_CHECK(glCompileShader(vert));
    GL_CHECK(glCompileShader(frag));
    GL_CHECK(glAttachShader(mProgram, vert));
    GL_CHECK(glAttachShader(mProgram, frag));
    GL_CHECK(glLinkProgram(mProgram));
	GLint isLinked = 0;
    GL_CHECK(glGetProgramiv(mProgram, GL_LINK_STATUS, (int *)&isLinked));
	/* Check if program is ready to use*/
	if (isLinked == GL_FALSE)
	{
		/* Something went wrong delete program and throw exception */
		GLint maxLength = 0;
        GL_CHECK(glGetProgramiv(mProgram, GL_INFO_LOG_LENGTH, &maxLength));
        std::vector<GLchar> infoLog(maxLength);
        GL_CHECK(glGetProgramInfoLog(mProgram, maxLength, &maxLength, &infoLog[0]));
        GL_CHECK(glDeleteProgram(mProgram));
        GL_CHECK(glDeleteShader(vert));
        GL_CHECK(glDeleteShader(frag));

		string reason(&infoLog[0]);
        qDebug() << "Shader isnt linked: " << reason.c_str();
	}

	/* Always detach shaders after a successful link */
    GL_CHECK(glDetachShader(mProgram, vert));
    GL_CHECK(glDetachShader(mProgram, frag));
}



SkyShader::SkyShader(const std::string& filename) : Shader(filename)
{
    // Get location of specific uniforms
    MVPloc = GLContext::gl->glGetUniformLocation(mProgram, "uModelViewProj");
    cubeMapLoc = GLContext::gl->glGetUniformLocation(mProgram, "uCubeMap");
}

void SkyShader::SetCubeMap(GLuint cubemap)
{
    // Set color texture as 0 unit
    GL_CHECK(glActiveTexture(GL_TEXTURE0));
    GL_CHECK(glBindTexture(GL_TEXTURE_CUBE_MAP, cubemap));
    GL_CHECK(glUniform1i(cubeMapLoc, 0));
}

TestShader::TestShader(const std::string& filename) : Shader(filename)
{
	/* Get location of specific uniforms */
    testMapLoc = GLContext::gl->glGetUniformLocation(mProgram, "uTestMap");
    // Get location of specific uniforms
    MVPloc = GLContext::gl->glGetUniformLocation(mProgram, "uViewProj");
}

void TestShader::SetTestMap(GLuint tex)
{
    GL_CHECK(glActiveTexture(GL_TEXTURE0));
    GL_CHECK(glBindTexture(GL_TEXTURE_2D, tex));
    GL_CHECK(glUniform1i(testMapLoc, 0));
}

ParticleShader::ParticleShader(const std::string& filename) : Shader(filename)
{
    Vloc = GLContext::gl->glGetUniformLocation(mProgram, "uView");
    Ploc = GLContext::gl->glGetUniformLocation(mProgram, "uProj");
    PRloc = GLContext::gl->glGetUniformLocation(mProgram, "uPointRadius");;
    PSloc = GLContext::gl->glGetUniformLocation(mProgram, "uPointScale");;
}

ThickShader::ThickShader(const std::string& filename) : Shader(filename)
{
    Vloc = GLContext::gl->glGetUniformLocation(mProgram, "uView");
    Ploc = GLContext::gl->glGetUniformLocation(mProgram, "uProj");
    PRloc = GLContext::gl->glGetUniformLocation(mProgram, "uPointRadius");;
    PSloc = GLContext::gl->glGetUniformLocation(mProgram, "uPointScale");;
}

BlurShader::BlurShader(const std::string& filename) : Shader(filename)
{
    Floc = GLContext::gl->glGetUniformLocation(mProgram, "uFar");
    Nloc = GLContext::gl->glGetUniformLocation(mProgram, "uNear");
    Depthloc = GLContext::gl->glGetUniformLocation(mProgram, "uDepthtex");
}

NormalShader::NormalShader(const std::string& filename) : Shader(filename)
{
    Floc = GLContext::gl->glGetUniformLocation(mProgram, "uFar");
    Nloc = GLContext::gl->glGetUniformLocation(mProgram, "uNear");
    IPloc = GLContext::gl->glGetUniformLocation(mProgram, "uInvProj");
    Posloc = GLContext::gl->glGetUniformLocation(mProgram, "uPositiontex");
    Depthloc = GLContext::gl->glGetUniformLocation(mProgram, "uDepthtex");
}

ResolveShader::ResolveShader(const std::string& filename) : Shader(filename)
{
    Vloc = GLContext::gl->glGetUniformLocation(mProgram, "uView");
    Floc = GLContext::gl->glGetUniformLocation(mProgram, "uFar");
    Nloc = GLContext::gl->glGetUniformLocation(mProgram, "uNear");
    IPloc = GLContext::gl->glGetUniformLocation(mProgram, "uInvProj");
    Posloc = GLContext::gl->glGetUniformLocation(mProgram, "uPositiontex");
    Depthloc = GLContext::gl->glGetUniformLocation(mProgram, "uDepthtex");
    Backgroundloc = GLContext::gl->glGetUniformLocation(mProgram, "uBackgroundtex");
    NormalLoc = GLContext::gl->glGetUniformLocation(mProgram, "uNormaltex");
    Thickloc = GLContext::gl->glGetUniformLocation(mProgram, "uThicktex");
    cubeMapLoc = GLContext::gl->glGetUniformLocation(mProgram, "uCubemaptex");
}

void ResolveShader::SetCubeMap(GLuint cubemap)
{
    // Set color texture as 0 unit
    GL_CHECK(glActiveTexture(GL_TEXTURE0));
    GL_CHECK(glBindTexture(GL_TEXTURE_CUBE_MAP, cubemap));
    GL_CHECK(glUniform1i(cubeMapLoc, 0));
}
