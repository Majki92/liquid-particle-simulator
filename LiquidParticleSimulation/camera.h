#ifndef CAMERA_H
#define CAMERA_H
#include "utils.h"

class Camera
{
public:
    Camera();
    ~Camera();

    /* Get/Set world camera position */
    glm::vec3 GetPosition() const;
    void SetPosition(float x, float y, float z);
    void SetPosition(const glm::vec3& v);

    /* Get camera basis vectors */
    glm::vec3 GetRight() const;
    glm::vec3 GetUp() const;
    glm::vec3 GetLook() const;

    /* Set frustum */
    void SetLens(float fovY, float aspect, float zn, float zf);

    /* Set look at */
    void LookAt(const glm::vec3& pos, const glm::vec3& target, const glm::vec3& up);

    /* Get View/Proj matrices */
    glm::mat4 View() const;
    glm::mat4 Proj() const;
    glm::mat4 InvProj();
    glm::mat4 ViewProj() const;

    /* Strafe/Walk the camera a distance d */
    void Strafe(float d);
    void Walk(float d);

    /* Rotate the camera */
    void Pitch(float angle);
    void RotateY(float angle);

    /* After modifying camera position/orientation rebuild view matrix */
    void UpdateViewMatrix();

private:
    /* Camera coordinate system with coordinates relative to world space */
    glm::vec3 mPosition;
    glm::vec3 mRight;
    glm::vec3 mUp;
    glm::vec3 mLook;

    /* Cache frustum properties */
    float mNearZ;
    float mFarZ;
    float mAspect;
    float mFovY;
    float mNearWindowHeight;
    float mFarWindowHeight;

    /* Cache View/Proj matrices */
    glm::mat4 mView;
    glm::mat4 mProj;
};

#endif
