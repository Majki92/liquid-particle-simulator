#-------------------------------------------------
#
# Project created by QtCreator 2015-10-20T19:12:41
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = LiquidParticleSimulation
TEMPLATE = app

CONFIG += c++11
#CONFIG += thread
CONFIG -= app_bundle
SOURCES += main.cpp\
        mainwindow.cpp \
    simulator.cpp \
    renderer.cpp \
    simulation/simulationthread.cpp \
    simulation/particlesimulatorcpp.cpp \
    utils.cpp \
    shader.cpp \
    renderobject.cpp \
    geometrygenerator.cpp \
    texturemgr.cpp \
    camera.cpp \
    simulation/iparticlesimulator.cpp \
    simulation/particlesimulatorcuda.cpp \
    simulation/particlesimulatoropenmp.cpp

HEADERS  += mainwindow.h \
    simulator.h \
    renderer.h \
    simulation/simulationthread.h \
    simulation/iparticlesimulator.h \
    simulation/particlesimulatorcpp.h \
    utils.h \
    shader.h \
    renderobject.h \
    geometrygenerator.h \
    texturemgr.h \
    camera.h \
    simulation/particlesimulatorcuda.h \
    simulation/particlesimulatoropenmp.h

INCLUDEPATH += $$PWD/vendor/glm/include

mac {
    Resources.files = $$PWD/media
    Resources.path = Contents/MacOS
    QMAKE_BUNDLE_DATA += Resources
}

win32 {
    PWD_WIN = $${PWD}
    DESTDIR_WIN = $${OUT_PWD}
    PWD_WIN ~= s,/,\\,g
    DESTDIR_WIN ~= s,/,\\,g

    copyfiles.commands = $$quote(cmd /c xcopy /S /Y /I $${PWD_WIN}\\media $${DESTDIR_WIN}\\media)

    QMAKE_EXTRA_TARGETS += copyfiles
    POST_TARGETDEPS += copyfiles

#Debug and Release flags necessary for compilation and linking
  QMAKE_CFLAGS_DEBUG += /MDd /openmp
  QMAKE_CXXFLAGS_DEBUG += /MDd /openmp
  QMAKE_CFLAGS_RELEASE += /MD /openmp
  QMAKE_CXXFLAGS_RELEASE += /MD /openmp
  MSVCRT_LINK_FLAG_DEBUG   = "/MDd"
  MSVCRT_LINK_FLAG_RELEASE = "/MD"

}

linux {
    copyfiles.commands = cp -r $$PWD/media $$OUT_PWD
    QMAKE_EXTRA_TARGETS += copyfiles
    POST_TARGETDEPS += copyfiles
}

FORMS    += mainwindow.ui

# CUDA settings <-- may change depending on your system
CUDA_SOURCES += simulation/particlesimulatorcuda.cu
CUDA_OBJECTS_DIR = release/cuda

# Path to cuda SDK install <-- may change depending on your system
CUDA_DIR = "C:/Program Files/NVIDIA GPU Computing Toolkit/CUDA/v7.5"
CUDA_SAMPLES_DIR = "C:/ProgramData/NVIDIA Corporation/CUDA Samples/v7.5/common/inc"

# Path to
## SYSTEM_TYPE - compiling for 32 or 64 bit architecture
SYSTEM_NAME = x64         # Depending on your system either 'Win32', 'x64', or 'Win64'
SYSTEM_TYPE = 64

CUDA_ARCH = sm_21         # Type of CUDA architecture, for example 'compute_10', 'compute_11', 'sm_10'
NVCC_OPTIONS = --use_fast_math

# include paths
INCLUDEPATH += $$CUDA_DIR/include
INCLUDEPATH += $$CUDA_SAMPLES_DIR

# library directories
QMAKE_LIBDIR += $$CUDA_DIR/lib/$$SYSTEM_NAME

# Add the necessary libraries
LIBS += -lcuda -lcudart

# SPECIFY THE R PATH FOR NVCC (this caused me a lot of trouble before)
#QMAKE_LFLAGS += -Wl,-rpath,$$CUDA_DIR"lib" # <-- added this
#NVCCFLAGS = -Xlinker -rpath,$$CUDA_DIR"lib" # <-- and this

# The following makes sure all path names (which often include spaces) are put between quotation marks
CUDA_INC = $$join(INCLUDEPATH,'" -I"','-I"','"')

# Configuration of the Cuda compiler
CONFIG(debug, debug|release) {
    # Debug mode
    cuda_d.input = CUDA_SOURCES
    cuda_d.output = $$CUDA_OBJECTS_DIR/${QMAKE_FILE_BASE}_cuda.o
    cuda_d.commands = "$$CUDA_DIR/bin/nvcc.exe" -D_DEBUG $$NVCC_OPTIONS $$CUDA_INC $$LIBS --machine $$SYSTEM_TYPE -arch=$$CUDA_ARCH -Xcompiler $$MSVCRT_LINK_FLAG_DEBUG -c -o ${QMAKE_FILE_OUT} ${QMAKE_FILE_NAME}
    cuda_d.dependency_type = TYPE_C
    QMAKE_EXTRA_COMPILERS += cuda_d
}
else {
    # Release mode
    cuda.input = CUDA_SOURCES
    cuda.output = $$CUDA_OBJECTS_DIR/${QMAKE_FILE_BASE}_cuda.o
    cuda.commands = $$CUDA_DIR/bin/nvcc.exe  $$NVCC_OPTIONS $$CUDA_INC $$LIBS --machine $$SYSTEM_TYPE -arch=$$CUDA_ARCH -Xcompiler $$MSVCRT_LINK_FLAG_RELEASE -c -o ${QMAKE_FILE_OUT} ${QMAKE_FILE_NAME}
    cuda.dependency_type = TYPE_C
    QMAKE_EXTRA_COMPILERS += cuda
}

DISTFILES += \
    simulation/particlesimulatorcuda.cu



