#ifndef TEXTUREMGR_H
#define TEXTUREMGR_H


#include "utils.h"
#include <string>
#include <map>

/* Namespace for managing textures */
class TextureMgr
{
public:
    static GLuint CreateTexture(std::string filename);
    static GLuint CreateCubeTexture(std::string filename);

private:
    static bool LoadTexture(std::string filename);
    static bool LoadCubeTexture(std::string filename);
    static std::map<std::string, GLuint> mTextureMap;
};


#endif // TEXTUREMGR_H
