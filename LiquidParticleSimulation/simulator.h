#ifndef SIMULATOR_H
#define SIMULATOR_H

#include <cstdint>
#include <QObject>
#include <QAtomicInteger>
#include "utils.h"

#include <simulation/simulationthread.h>

#ifndef M_PI
#define M_PI 3.14159265359f
#endif

struct ParticleCollection {
    static const int MAX_PARTICLES = 100000;

    //particle data start
    glm::vec3 pos[MAX_PARTICLES];
    glm::vec3 speed[MAX_PARTICLES];
    glm::vec3 accel[MAX_PARTICLES];
    //glm::vec3 force[MAX_PARTICLES];
    float density[MAX_PARTICLES];
    float pressure[MAX_PARTICLES];
    //particle data end

    QAtomicInteger<uint32_t> count;
    GLuint m_vbCPU;
    GLuint m_vaoCPU;
    GLuint m_vbGPU;
    GLuint m_vaoGPU;
    bool m_gpu;
};

class Simulator : public QObject
{
    Q_OBJECT
public:
    explicit Simulator(QObject *parent = 0);
    ~Simulator() {emit TerminateSig(); m_simThread.Stop(); m_simThread.terminate();}

    void Start() { emit StartSig(); }
    void Stop() { emit StopSig(); }
    void Reset(SimImplType implType) { emit ResetSig(implType); }

    bool IsInProgress() { return m_simThread.IsInProgress(); }
    uint32_t GetSpeed() { return m_simThread.GetSpeed(); }
    double GetFrameRenderingTime() { return m_simThread.GetFrameRenderingTime(); }
    void SetSpeed(uint32_t speed) { emit SetSpeedSig(speed); }

    ParticleCollection* m_particles;
private:
    SimulationThread m_simThread;

signals:
    void StopSig();
    void StartSig();
    void TerminateSig();
    void ResetSig(SimImplType implType);
    void SetSpeedSig(uint32_t speed);
};

#endif // SIMULATOR_H
