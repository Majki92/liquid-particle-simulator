#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QDebug"
#include <QTimer>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_simulator(this)
{
    ui->setupUi(this);
    ui->startButton->setEnabled(false);
    ui->openGLWidget->SetCollection(m_simulator.m_particles);
    ui->openGLWidget->SetStartButton(ui->startButton);
    ui->speedText->setText(QString::number(m_simulator.GetSpeed()));

    QObject::connect(ui->startButton,SIGNAL(clicked()),this,SLOT(OnStartBtnClk()));
    QObject::connect(ui->resetButton,SIGNAL(clicked()),this,SLOT(OnResetBtnClk()));
    QObject::connect(ui->speedSlider,SIGNAL(sliderMoved(int)),this,SLOT(OnSpeedSliderMove(int)));
    QObject::connect(ui->radioCpp,SIGNAL(toggled(bool)),this,SLOT(onCppRadioToggled()));
    QObject::connect(ui->radioOpenCL,SIGNAL(toggled(bool)),this,SLOT(on_radioOpenCL_toggled(bool)));

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(Update()));
    timer->start(100);
}

MainWindow::~MainWindow()
{
    delete timer;
    delete ui;
}

void MainWindow::OnSpeedSliderMove(int position) {
   m_simulator.SetSpeed(((position) * MAX_PARTICLES_PER_SEC) / 100);
   ui->speedText->setText(QString::number(m_simulator.GetSpeed()));
}

void MainWindow::OnStartBtnClk() {
    if (!m_simulator.IsInProgress()) {
        qDebug() << "Start Simulation";
        m_simulator.Start();
        ui->startButton->setText(QString("Stop Simulation"));
        ui->radioCpp->setEnabled(false);
        ui->radioCUDA->setEnabled(false);
        ui->radioOpenCL->setEnabled(false);
        ui->resetButton->setEnabled(false);
    } else {
        qDebug() << "Stop Simulation";
        m_simulator.Stop();
        ui->startButton->setText(QString("Start Simulation"));
        ui->radioCpp->setEnabled(true);
        ui->radioCUDA->setEnabled(true);
        ui->radioOpenCL->setEnabled(true);
        ui->resetButton->setEnabled(true);
    }
}

void MainWindow::OnResetBtnClk() {
    qDebug() << "Reset Simulation";
    if (ui->radioCpp->isChecked())
        m_simulator.Reset(SimImplType::CPP);
    else if (ui->radioOpenCL->isChecked())
        m_simulator.Reset(SimImplType::OPENMP);
    else if (ui->radioCUDA->isChecked())
        m_simulator.Reset(SimImplType::CUDA);
    else
        m_simulator.Reset(SimImplType::NONE);
}

void MainWindow::onCppRadioToggled() {
    //ui->openGLWidget->SwitchSimulator(ui->radioCpp->isChecked());
    m_simulator.Reset(SimImplType::CPP);
}

void MainWindow::Update() {
    ui->countText->setText(QString::number(m_simulator.m_particles->count.load()));
    ui->frameTime_text->setText(QString::number(m_simulator.GetFrameRenderingTime()));
    timer->start(100);
}

void MainWindow::on_radioOpenCL_toggled(bool checked)
{
    if (checked)
        m_simulator.Reset(SimImplType::OPENMP);
}

void MainWindow::on_radioCUDA_toggled(bool checked)
{
    if (checked)
        m_simulator.Reset(SimImplType::CUDA);
}
