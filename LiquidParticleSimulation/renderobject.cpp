#include "renderobject.h"
#include "geometrygenerator.h"
#include "texturemgr.h"
#include <simulator.h>

using namespace std;

#define BUFFER_OFFSET(i) ((char *)NULL + (i))

RenderObject::RenderObject()
{
    GL_CHECK(glGenBuffers(1, &mVB));
    GL_CHECK(glGenBuffers(1, &mIB));
    GL_CHECK(glGenVertexArrays(1, &mVAO));
}

Sky::Sky(const std::string& cubemapFilename, float skySphereRadius) : RenderObject()
{
    /* Create texture */
    mTexture = TextureMgr::CreateCubeTexture(cubemapFilename);

    /* Create geometry */
    std::vector<glm::vec4> vertices;
    std::vector<GLuint> indices;
    GeometryGenerator::createSphereSky(skySphereRadius, 30, 30, &vertices, &indices);

    /* Send data to GPU */
    GL_CHECK(glBindVertexArray(mVAO));
    GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, mVB));
    GL_CHECK(glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec4) * vertices.size(), &vertices[0], GL_STATIC_DRAW));
    GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, 0));
    GL_CHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIB));
    GL_CHECK(glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * indices.size(), &indices[0], GL_STATIC_DRAW));
    GL_CHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0));
    GL_CHECK(glBindVertexArray(0));
    mCount = indices.size();
}

Particle::Particle(ParticleCollection* particles) : RenderObject()
{
    /* Send data to GPU */
    mCount = particles->count;
    GL_CHECK(glBindVertexArray(mVAO));
    GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, mVB));
    GL_CHECK(glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * mCount, &(particles->pos[0]), GL_STATIC_DRAW));
    GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, 0));
    GL_CHECK(glBindVertexArray(0));

}

Plane::Plane(const std::string& mapFilename, int sizeX, int sizeY) : RenderObject()
{
    /* Create texture */
    mTexture = TextureMgr::CreateTexture(mapFilename);

    /* Create geometry */
    std::vector<planeVertex> vertices;
    std::vector<GLuint> indices;
    GeometryGenerator::createPlane(glm::vec2(sizeX, sizeY), &vertices, &indices);
    mCount = indices.size();
    /* Send data to GPU */
    GL_CHECK(glBindVertexArray(mVAO));
    GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, mVB));
    GL_CHECK(glBufferData(GL_ARRAY_BUFFER, sizeof(planeVertex) * vertices.size(), &vertices[0], GL_STATIC_DRAW));
    GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, 0));
    GL_CHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIB));
    GL_CHECK(glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * indices.size(), &indices[0], GL_STATIC_DRAW));
    GL_CHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0));
    GL_CHECK(glBindVertexArray(0));
}
