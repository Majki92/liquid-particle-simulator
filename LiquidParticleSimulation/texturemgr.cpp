#include "texturemgr.h"
#include <QDebug>
#include <QOpenGLWidget>

using namespace std;

map<std::string, GLuint> TextureMgr::mTextureMap;

GLuint TextureMgr::CreateTexture(string filename)
{
    GLuint texture;
    /* If exists return that texture */
    if (mTextureMap.find(filename) != mTextureMap.end())
    {
        texture = mTextureMap[filename];
    }
    else
    {
        /* Generate texture */
        GL_CHECK(glGenTextures(1, &texture));
        GL_CHECK(glBindTexture(GL_TEXTURE_2D, texture));

        /* Can't open texture file */
        if (!LoadTexture(filename))
        {
            qDebug() << "Unable to load cube texture from file: " << filename.c_str() << endl;
            GL_CHECK(glBindTexture(GL_TEXTURE_CUBE_MAP, 0));
            return -1;
        }

        /* Set sampling parameters */
        GL_CHECK(glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
        GL_CHECK(glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));
        GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
        GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));
        GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE));
        GL_CHECK(glBindTexture(GL_TEXTURE_2D, 0));
        mTextureMap[filename] = texture;
    }

    return texture;
}

GLuint TextureMgr::CreateCubeTexture(string filename)
{
    GLuint texture;

    /* If exists return that texture */
    if (mTextureMap.find(filename) != mTextureMap.end())
    {
        texture = mTextureMap[filename];
    }
    else
    {
        /* Generate texture */
        GL_CHECK(glGenTextures(1, &texture));
        GL_CHECK(glBindTexture(GL_TEXTURE_CUBE_MAP, texture));

        /* Try to load from file */
        if (!LoadCubeTexture(filename))
        {
            qDebug() << "Unable to load cube texture from file: " << filename.c_str() << endl;
            GL_CHECK(glBindTexture(GL_TEXTURE_CUBE_MAP, 0));
            return -1;
        }
        /* Set sampling parameters */
        GL_CHECK(glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR));
        GL_CHECK(glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
        GL_CHECK(glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
        GL_CHECK(glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));
        GL_CHECK(glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE));
        GL_CHECK(glBindTexture(GL_TEXTURE_CUBE_MAP, 0));
        mTextureMap[filename] = texture;
    }

    return texture;
}

bool TextureMgr::LoadTexture(string filename)
{

    string file;
    file.append(filename);
    QImage tmpImage(file.c_str());
    QImage image = tmpImage.mirrored();
    if(image.isNull())
        return false;
    GL_CHECK(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.width(), image.height(), 0, GL_BGRA, GL_UNSIGNED_BYTE, image.bits()));
    return true;
}

/* Same as above but fore cube textures */
bool TextureMgr::LoadCubeTexture(string filename)
{
    static GLenum sides[] = { GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
                              GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
                              GL_TEXTURE_CUBE_MAP_NEGATIVE_X, GL_TEXTURE_CUBE_MAP_POSITIVE_X };
    for (int i = 0; i < 6; ++i)
    {
        string file;
        file.append(filename);
        file.insert(file.find_last_of("."), 1, (char)(((int)'0') + i));
        //qDebug() << file.c_str();
        QImage tmpImage(file.c_str());
        QImage image = tmpImage.mirrored();
        if(image.isNull()){
            qDebug() << "Error loading cube texture.";
            return false;
        }
        GL_CHECK(glTexImage2D(sides[i], 0, GL_RGBA, image.width(), image.height(), 0, GL_BGRA, GL_UNSIGNED_BYTE, image.bits()));
    }

    return true;
}
