#version 410

in vec4 fPosition;
in vec3 fPosEye;

uniform mat4 uView;
uniform mat4 uProj;
uniform float uPointRadius;  // point size in world space

out vec4 fColor;

void main(void)
{
    // calculate normal from texture coordinates
    vec3 N;
	
    N.xy = gl_PointCoord.xy * vec2(2.0f, -2.0f) + vec2(-1.0f, 1.0f);
    float mag = dot(N.xy, N.xy);
    if (mag > 1.0) discard;   // kill pixels outside circle
    N.z = sqrt(1.0-mag);
    
    //calculate depth
    vec4 pixelPos = vec4(fPosEye + normalize(N) * uPointRadius, 1.0f);
    vec4 clipSpacePos = uProj * pixelPos;
    gl_FragDepth = (clipSpacePos.z / clipSpacePos.w)*0.5+0.5;
   // float diffuse = max(0.0, dot(N, vec3(0.0, -1.0, 0.0)));
	fColor = pixelPos;//diffuse * vec4(0.2f, 0.5f, 0.2f, 1.0f);
}
