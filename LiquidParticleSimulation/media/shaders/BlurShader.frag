#version 410

in vec2 fTexcoord;

uniform sampler2D uDepthtex;

uniform float uFar;
uniform float uNear;

out vec4 fColor;

//Depth used in the Z buffer is not linearly related to distance from camera
//This restores linear depth
float linearizeDepth(float exp_depth, float near, float far) {
    return	(2 * near) / (far + near -  exp_depth * (far - near)); 
}

void main()
{       
    //Get Depth Information about the Pixel
    float exp_depth = texture(uDepthtex,fTexcoord).r;
    float lin_depth = linearizeDepth(exp_depth,uNear,uFar);
    float blurRadius = (1.0f/lin_depth) * 0.0002f;
    int windowWidth = 5;
    float sum = 0;
    float wsum = 0;
    
    if(exp_depth > 0.99f)
	{
		fColor = vec4(exp_depth);
		return;
    }
    
    for(int x = -windowWidth; x < windowWidth; x++)
	{
		for(int y = -windowWidth; y < windowWidth; y++)
		{
			vec2 sampleV = vec2(fTexcoord.s + x*blurRadius, fTexcoord.t + y*blurRadius);
			float sampleDepth = texture(uDepthtex, sampleV).r;
			
			if(sampleDepth < 0.99f)
			{
				//Spatial
				float r = length(vec2(x,y)) * 0.0001f;
				float w = exp(-r*r);
			
				//Range
				float r2 = (sampleDepth-exp_depth) * 200.0f;
				float g = exp(-r2*r2);
			
				sum += sampleDepth * w * g ;
				wsum += w * g;
			}
		}
    }
    
    if(wsum > 0.0f){
		sum = sum/wsum;
    }
    
    fColor = vec4(sum);
}
