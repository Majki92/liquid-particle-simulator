#version 410 core

in vec2 fTexCoord;

uniform sampler2D uTestMap;

out vec4 fColor;

void main()
{
	fColor = vec4(texture(uTestMap, fTexCoord).rgb, 1.0);
}