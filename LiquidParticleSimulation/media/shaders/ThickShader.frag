#version 410

out vec4 fColor;

void main(void)
{
    // calculate normal from texture coordinates
    vec3 N;
	
    N.xy = gl_PointCoord.xy * vec2(2.0f, -2.0f) + vec2(-1.0f, 1.0f);
    float mag = dot(N.xy, N.xy);
    if (mag > 1.0) discard;   // kill pixels outside circle

	//Gaussian Distribution
	float dist = length(gl_PointCoord.xy-vec2(0.5f,0.5f));
	float sigma = 3.0f;
	float mu = 0.0f;
	float g_dist = 0.02f * exp(-(dist-mu)*(dist-mu)/(2*sigma));
	
	fColor = vec4(g_dist);
}