#version 410

layout(location = 0) in vec3 vPositionH;
layout(location = 1) in vec2 vTexcoord;

out vec2 fTexcoord;

void main()
{
	fTexcoord = vTexcoord;
	gl_Position = vec4(vPositionH, 1.0f);
}