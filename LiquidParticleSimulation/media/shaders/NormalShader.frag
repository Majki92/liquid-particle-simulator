#version 410

in vec2 fTexcoord;

uniform mat4 uInvProj;

uniform sampler2D uDepthtex;

uniform float uFar;
uniform float uNear;

out vec4 fColor;

//Depth used in the Z buffer is not linearly related to distance from camera
//This restores linear depth

float linearizeDepth(float exp_depth, float near, float far) {
    return	(2 * near) / (far + near -  exp_depth * (far - near)); 
}

vec3 uvToEye(vec2 texCoord, float depth){
	float x = texCoord.x * 2.0 - 1.0;
	float y = texCoord.y * 2.0 - 1.0;
	vec4 clipPos = vec4(x , y, depth, 1.0f);
	vec4 viewPos = uInvProj * clipPos;
	return viewPos.xyz / viewPos.w;
}

vec3 getEyePos(in vec2 texCoord){
	float exp_depth = texture(uDepthtex,fTexcoord).r;
    float lin_depth = linearizeDepth(exp_depth,uNear,uFar);
    return uvToEye(texCoord,lin_depth);
}

void main()
{       
    //Get Depth Information about the Pixel
    float exp_depth = texture(uDepthtex,fTexcoord).r;

    //float lin_depth = linearizeDepth(exp_depth,uNear,uFar);
	vec3 position = uvToEye(fTexcoord, exp_depth);
	
	//Compute Gradients of Depth and Cross Product Them to Get Normal
	fColor = vec4(normalize(cross(dFdx(position.xyz), dFdy(position.xyz))), 1.0f);
}