#version 410 core

layout(location = 0) in vec4 vPositionW;
layout(location = 1) in vec2 vTexCoord;

uniform mat4 uViewProj;

out vec2 fTexCoord;

void main()
{
	fTexCoord = vTexCoord;
	gl_Position = uViewProj * vPositionW;
}