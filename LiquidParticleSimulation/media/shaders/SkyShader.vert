#version 410 core

layout(location = 0) in vec4 vPositionL;

uniform mat4 uModelViewProj;

out vec3 fTexCoord;

void main()
{
	fTexCoord = vPositionL.xyz;
	gl_Position = uModelViewProj * vPositionL;
}