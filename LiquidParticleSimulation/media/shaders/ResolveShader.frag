#version 410

in vec2 fTexcoord;

uniform mat4 uView;
uniform mat4 uInvProj;

uniform sampler2D uDepthtex;
uniform sampler2D uNormaltex;
uniform sampler2D uPositiontex;
uniform sampler2D uBackgroundtex;
uniform sampler2D uThicktex;

uniform samplerCube uCubemaptex;

uniform float uFar;
uniform float uNear;

out vec4 fColor;

//Depth used in the Z buffer is not linearly related to distance from camera
//This restores linear depth

float linearizeDepth(float exp_depth, float near, float far) {
    return	(2 * near) / (far + near -  exp_depth * (far - near)); 
}

vec3 uvToEye(vec2 texCoord, float depth){
	float fovy = radians(60.0);
	float aspect = 640.0f / 480.0f;
	float invFocalLenX   = tan(fovy * 0.5) * aspect;
	float invFocalLenY   = tan(fovy * 0.5);
	//vec2 uv = (texCoord * vec2(2.0,-2.0) - vec2(1.0,-1.0));
	//return vec3(uv * vec2(invFocalLenX, invFocalLenY) * depth, depth);
	
	float x = texCoord.x * 2.0 - 1.0;
	float y = texCoord.y * -2.0 + 1.0;
	vec4 clipPos = vec4(x , y, depth, 1.0f);
	vec4 viewPos = uInvProj * clipPos;
	return viewPos.xyz / viewPos.w;
}


void main()
{
	//Uniform Light Direction (Billboard)
    vec4 lightDir = uView * vec4(0.7f, 1.0f, 0.0f, 0.0f);
        
    //Get Texture Information about the Pixel
    vec3 N = texture(uNormaltex,fTexcoord).xyz;
    float exp_depth = texture(uDepthtex,fTexcoord).r;
   
    vec3 BackColor = texture(uBackgroundtex,fTexcoord).xyz;
	vec3 position = texture(uPositiontex,fTexcoord).xyz;
	float thickness = clamp(texture(uThicktex,fTexcoord).r,0.0f,1.0f);
    
    vec3 incident = normalize(lightDir.xyz);
    vec3 viewer = normalize(-position.xyz);
    
    //Blinn-Phong Shading Coefficients
    vec3 H = normalize(incident + viewer);
    float specular = pow(max(0.0f, dot(H,N)),50.0f);
    float diffuse = max(0.0f, dot(incident, N));
    
    //Background Only Pixels
    if(exp_depth > 0.99f){
		fColor = vec4(BackColor.rgb,1.0f);
		return;
	}
    
    //Fresnel Reflection
    float r_0 = 0.3f;
    float fres_refl = r_0 + (1-r_0)*pow(1-dot(N,viewer),5.0f);
    
    //Cube Map Reflection Values
    vec3 reflect = reflect(-viewer,N);
    vec4 refl_color = texture(uCubemaptex, reflect);
    
    //Color Attenuation from Thickness
    //(Beer's Law)
    float k_r = 5.0f;
    float k_g = 1.0f;
    float k_b = 0.1f;
    vec3 color_atten = vec3( exp(-k_r*thickness), exp(-k_g*thickness), exp(-k_b*thickness));
    
    //Background Refraction
    vec4 refrac_color = texture(uBackgroundtex, fTexcoord + N.xy*thickness);
    
    //Final Real Color Mix
    float transparency = 1-thickness;
    vec3 final_color = mix(color_atten.rgb * diffuse, refrac_color.rgb, transparency);

	//fColor = refl_color;
	fColor = vec4(final_color.rgb + specular * vec3(1.0f) + refl_color.rgb * fres_refl, 1.0f);
}