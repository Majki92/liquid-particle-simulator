#version 410 core

in vec3 fTexCoord;

uniform samplerCube uCubeMap;

out vec4 fColor;

void main () 
{
	fColor = texture(uCubeMap, fTexCoord);
}