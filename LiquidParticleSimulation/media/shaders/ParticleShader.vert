#version 410

layout(location = 0) in vec4 vPositionW;

uniform mat4 uView;
uniform mat4 uProj;
uniform float uPointRadius;  // point size in world space
uniform float uPointScale;   // scale to calculate size in pixels

out vec4 fPosition;
out vec3 fPosEye;

void main(void) 
{
	vec3 posEye = (uView * vec4(vPositionW.xyz, 1.0f)).xyz;
	float dist = length(posEye);
	gl_PointSize = uPointRadius * (uPointScale / dist);
	
	fPosEye = posEye;
	fPosition = uView * vec4(vPositionW.xyz, 1.0);
	gl_Position = uProj * uView * vec4(vPositionW.xyz, 1.0);
}