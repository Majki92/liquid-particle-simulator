

#include "particlesimulatorcuda.h"


#include <simulator.h>
#include <vector>
#include <QDebug>
#include <cmath>

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <cuda.h>
#include <device_functions.h>
#include <cuda_runtime_api.h>
#include <helper_cuda.h>
#include <helper_functions.h>

ParticleSimulatorCuda::ParticleSimulatorCuda(ParticleCollection* collection)
    : IParticleSimulator(collection)
{
    cudaDeviceProp prop;
    cudaGetDeviceProperties(&prop, 0);
    m_maxThreads = prop.maxThreadsDim[0];

    checkCudaErrors(cudaSetDevice(0));

    checkCudaErrors(cudaMalloc((void**) &(sm_particles.d_pos), sizeof(float3) * m_particles->MAX_PARTICLES));
    checkCudaErrors(cudaMalloc((void**) &(sm_particles.d_speed), sizeof(float3) * m_particles->MAX_PARTICLES));
    checkCudaErrors(cudaMalloc((void**) &(sm_particles.d_accel), sizeof(float3) * m_particles->MAX_PARTICLES));
    checkCudaErrors(cudaMalloc((void**) &(sm_particles.d_density), sizeof(float) * m_particles->MAX_PARTICLES));
    checkCudaErrors(cudaMalloc((void**) &(sm_particles.d_pressure), sizeof(float) * m_particles->MAX_PARTICLES));

    checkCudaErrors(cudaMalloc((void**) &(sm_particles.d_container_pos), sizeof(float3) * 5));
    checkCudaErrors(cudaMalloc((void**) &(sm_particles.d_container_norm), sizeof(float3) * 5));

    checkCudaErrors(cudaMemcpy((void*)sm_particles.d_container_pos,(void*)s_container_pos,sizeof(float3) * 5, cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy((void*)sm_particles.d_container_norm,(void*)s_container_norm,sizeof(float3) * 5, cudaMemcpyHostToDevice));
}

ParticleSimulatorCuda::~ParticleSimulatorCuda()
{
    checkCudaErrors(cudaFree(sm_particles.d_pos));
    checkCudaErrors(cudaFree(sm_particles.d_speed));
    checkCudaErrors(cudaFree(sm_particles.d_accel));
    checkCudaErrors(cudaFree(sm_particles.d_density));
    checkCudaErrors(cudaFree(sm_particles.d_pressure));

    checkCudaErrors(cudaFree(sm_particles.d_container_pos));
    checkCudaErrors(cudaFree(sm_particles.d_container_norm));
    checkCudaErrors(cudaDeviceReset());
}

void ParticleSimulatorCuda::Update(float dt, uint32_t speed) {

    // create new particles
    uint32_t added = CreateParticle(dt, speed, m_particles->count, m_particles->MAX_PARTICLES);
    uint32_t count = m_particles->count + added;
    //qDebug() << "After creating ";

    // here starts CUDA kernel 1
    // all needed parameters are copied
    ComputeDensityAndPressure(count);
    //qDebug() << "After computing density and pressure ";

    // here starts CUDA kernel 2
    ComputeForces(count);
    //qDebug() << "After computing forces ";

    // here starts CUDA kernel 3
    // update postion
    UpdatePosition(dt, count);
    //qDebug() << "Updated: " << m_particles->count;
\
    // copy pos data back to CPU ad up the count
    cudaMemcpy((void*)m_particles->pos,sm_particles.d_pos,sizeof(float3)*count,cudaMemcpyDeviceToHost);
    m_particles->count.fetchAndAddRelaxed(added);
}








