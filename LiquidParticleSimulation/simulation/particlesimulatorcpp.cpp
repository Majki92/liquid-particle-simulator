#include "particlesimulatorcpp.h"
#include <simulator.h>
#include <vector>
#include <QDebug>
#include <cmath>

ParticleSimulatorCpp::ParticleSimulatorCpp(ParticleCollection* collection)
    : IParticleSimulator(collection)
{}

void ParticleSimulatorCpp::Update(float dt, uint32_t speed) {
    //qDebug() << "Update( " << dt << " ) ";

    // create new particles
    m_particlesLeft2Create += ((float)speed) * dt;
    uint32_t parts2Create = (uint32_t)m_particlesLeft2Create;
    m_particlesLeft2Create -= parts2Create;
    AddParticles(parts2Create);

    // here starts kernel 1
    ComputeDensityAndPressure();
    // here starts kernel 2
    ComputeForces();

    // here starts kernel 3
    // update postion
    uint32_t p_count = m_particles->count;
    for(int i=0; i<p_count; ++i) {
        glm::vec3 n_pos = m_particles->pos[i] + m_particles->speed[i] * dt + m_particles->accel[i] * dt * dt;
        glm::vec3 n_speed = (n_pos - m_particles->pos[i])/dt;
        m_particles->speed[i] = n_speed;
        m_particles->pos[i] = n_pos;
    }
}

//////// device helper functions - block start
float poly6(float dist2) {
    static const float coeff = 315.f/(64.f*M_PI*pow(IParticleSimulator::s_search_radius,9));
    static const float search_radius2 = IParticleSimulator::s_search_radius*IParticleSimulator::s_search_radius;
    return coeff * pow(search_radius2 - dist2, 3);
}

glm::vec3 poly6Gradient(glm::vec3 dist_vec, float dist2) {
    static const float coeff = -945.f/(32.f*M_PI*pow(IParticleSimulator::s_search_radius,9));
    static const float search_radius2 = IParticleSimulator::s_search_radius*IParticleSimulator::s_search_radius;
    return coeff * (float)pow(search_radius2 - dist2, 2) * dist_vec;
}

glm::vec3 spikyGradient(glm::vec3 dist_vec, float dist2) {
    static const float coeff = 15.f/(M_PI*pow(IParticleSimulator::s_search_radius,6));
    float dist = glm::sqrt(dist2);
    return coeff * (float)pow(IParticleSimulator::s_search_radius - dist,3) * dist_vec / dist;
}

float poly6Laplacian(float dist2) {
    static const float coeff = -945.f/(32.f*M_PI*pow(IParticleSimulator::s_search_radius,9));
    static const float search_radius2 = IParticleSimulator::s_search_radius*IParticleSimulator::s_search_radius;
    return coeff * (search_radius2 - dist2) * (3.f*search_radius2 - 7.f*dist2);
}

float viscosityLaplacian(float dist2) {
    static const float coeff = 45.f/(M_PI*pow(IParticleSimulator::s_search_radius,6));
    return coeff*(IParticleSimulator::s_search_radius - glm::sqrt(dist2));
}
//////// device helper functions - block end

void ParticleSimulatorCpp::ComputeDensityAndPressure() {
    const uint32_t p_count = m_particles->count;
    static const float search_radius2 = s_search_radius*s_search_radius;

    for(int i=0; i<p_count; ++i) {
        m_particles->density[i] = 0;

        for(int j=0; j<p_count; ++j) {
            glm::vec3 dist_vec = m_particles->pos[i] - m_particles->pos[j];
            float dist2 = glm::dot(dist_vec,dist_vec);

            if(dist2 <= search_radius2) {
                m_particles->density[i] += poly6(dist2);
            }
        }

        m_particles->density[i] *= s_mass;
        m_particles->pressure[i] = s_gas_stiffness * (m_particles->density[i] - s_rest_density);
    }
}

void ParticleSimulatorCpp::ComputeForces() {
    const uint32_t p_count = m_particles->count;
    static const float search_radius2 = s_search_radius*s_search_radius;

    for(int i=0; i<p_count; ++i) {
        glm::vec3 f_pressure(0,0,0);
        glm::vec3 f_viscosity(0,0,0);
        glm::vec3 f_surface(0,0,0);
        glm::vec3 f_gravity(0,-s_gravity,0);
        glm::vec3 color_field_norm;
        float color_field_lapiacian;

        for(int j=0; j<p_count; ++j) {
            glm::vec3 dist_vec = m_particles->pos[i] - m_particles->pos[j];
            float dist2 = glm::dot(dist_vec,dist_vec);

            if(dist2 <= search_radius2) {
                glm::vec3 gradient = poly6Gradient(dist_vec,dist2);
                glm::vec3 spiky_gradient = spikyGradient(dist_vec,dist2);
                if(i!=j) {
                    //f_pressure += (m_particles->pressure[i]/(float)pow(m_particles->density[i],2)
                                   //+ m_particles->pressure[j]/(float)pow(m_particles->density[j],2)) * spiky_gradient;
                    f_pressure += (m_particles->pressure[i] + m_particles->pressure[j]) / (2.0f * m_particles->density[j]) * spiky_gradient;
                    f_viscosity += (m_particles->speed[j] - m_particles->speed[i]) * viscosityLaplacian(dist2) / m_particles->density[j];
                }

                color_field_norm += gradient / m_particles->density[j];
                color_field_lapiacian += poly6Laplacian(dist2) / m_particles->density[j];
            }
        }

        f_pressure *= -s_mass;//*m_particles->density[i];
        f_viscosity *= s_viscosity*s_mass;

        color_field_norm *= s_mass;
        color_field_lapiacian *= s_mass;

        float color_field_len = glm::length(color_field_norm);
        if( color_field_len > s_surface_treshold) {
            f_surface = -s_surface_tension * color_field_norm / color_field_len * color_field_lapiacian;
        }

        m_particles->accel[i] = (f_pressure + f_viscosity + f_surface) / m_particles->density[i] + f_gravity;

        //this can be inlined
        CollisionForce(i);
    }
}

void ParticleSimulatorCpp::CollisionForce(uint32_t particle_id) {
    for(int k=0; k<5; ++k) {
        float d = glm::dot((s_container_pos[k] - m_particles->pos[particle_id]),s_container_norm[k]) + s_radius;
        if(d > 0.f) {
            m_particles->accel[particle_id] += s_wall_k * s_container_norm[k] * d;
            m_particles->accel[particle_id] -= s_wall_damp * glm::dot(m_particles->speed[particle_id],s_container_norm[k]) * s_container_norm[k];
        }
    }
}

