#include "simulationthread.h"

#include <simulation/particlesimulatorcpp.h>
#include <simulation/particlesimulatoropenmp.h>
#include <simulation/particlesimulatorcuda.h>
#include <ctime>

SimulationThread::SimulationThread(ParticleCollection *collection, QThread *parent)
    : QThread(parent)
{
    m_isInProgress = false;
    m_quit = false;
    m_simImplType = SimImplType::CPP;
    m_particleSpeed = MAX_PARTICLES_PER_SEC / 100;
    m_frameRenderingTime = 0;

    m_particleSimulators[SimImplType::CPP] = new ParticleSimulatorCpp(collection);
    m_particleSimulators[SimImplType::OPENMP] = new ParticleSimulatorOpenMP(collection);
    m_particleSimulators[SimImplType::CUDA] = new ParticleSimulatorCuda(collection);
}

void SimulationThread::run() {
    //m_isInProgress = true;
    m_lastUpdateTime = std::chrono::system_clock::now();
    while(true){
        bool var = m_isInProgress;
    if(m_isInProgress) {
        auto tmp = std::chrono::system_clock::now();
        std::chrono::duration<float> deltaTime = tmp - m_lastUpdateTime;
        m_lastUpdateTime = tmp;
        m_frameRenderingTime = deltaTime.count() * 1000;
       // qDebug() << "Simulation time: " << m_frameRenderingTime << " ms";
        m_particleSimulators[m_simImplType]->Update(std::max(std::min(deltaTime.count(), 0.01f), 0.001f), m_particleSpeed);
    } else {
        usleep(100000);
    }
        var = m_quit;
    if(m_quit)
        break;
    }
}

void SimulationThread::Reset(SimImplType implType)
{
    if(!m_isInProgress)
    {
        m_simImplType = implType;
        m_particleSimulators[m_simImplType]->Reset();
        m_frameRenderingTime = 0;
    }
}
