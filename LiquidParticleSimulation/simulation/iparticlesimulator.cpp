#include <simulation/iparticlesimulator.h>
#include <simulator.h>

// particle params
const float IParticleSimulator::s_radius = 0.05f;
const float IParticleSimulator::s_mass =  IParticleSimulator::s_radius*IParticleSimulator::s_radius*M_PI;
const float IParticleSimulator::s_search_radius = 4.57f * IParticleSimulator::s_radius;
// container params
const float IParticleSimulator::s_emiter_height = 5.0f;
const float IParticleSimulator::s_wall_damp = 0.9f;
const float IParticleSimulator::s_wall_k = 10000.0f;
const glm::vec3 IParticleSimulator::s_min_bound = glm::vec3(-6, 0,-1);
const glm::vec3 IParticleSimulator::s_max_bound = glm::vec3(6, 0,1);
const glm::vec3 IParticleSimulator::s_container_pos[5] = {glm::vec3(IParticleSimulator::s_min_bound.x,0,0),
                                                      glm::vec3(IParticleSimulator::s_max_bound.x,0,0),
                                                      glm::vec3(0,0,IParticleSimulator::s_min_bound.z),
                                                      glm::vec3(0,0,IParticleSimulator::s_max_bound.z),
                                                      glm::vec3(0,IParticleSimulator::s_min_bound.y,0)}; // Left, Right, Back, Front, Bottom
const glm::vec3 IParticleSimulator::s_container_norm[5] = {glm::vec3(1,0,0),glm::vec3(-1,0,0),glm::vec3(0,0,1),
                                                                 glm::vec3(0,0,-1),glm::vec3(0,1,0)};
// fluid params
const float IParticleSimulator::s_gravity = 9.81f;
const float IParticleSimulator::s_gas_stiffness = 3.0f;
const float IParticleSimulator::s_rest_density = 998.29f;
const float IParticleSimulator::s_viscosity = 0.55f;
const float IParticleSimulator::s_surface_tension = 0.0728f;
const float IParticleSimulator::s_surface_treshold = 0.07065f / IParticleSimulator::s_radius;

IParticleSimulator::IParticleSimulator(ParticleCollection* collection)
    : m_particles(collection), m_particlesLeft2Create(0.f), m_pos(0,s_emiter_height,0)
{
    m_particles->count.fetchAndStoreRelaxed(0);
}

void IParticleSimulator::AddParticles(uint32_t count) {
    if (count + m_particles->count > m_particles->MAX_PARTICLES)
        return;

    static int internal_counter = 0;

    for(int i=0; i< count; ++i) {
        float x = (float)(internal_counter % 3 - 1) / 5.f;
        float y = (float)(internal_counter % 3 - 1) / -5.f;
        float z = (float)((internal_counter/3) % 3 -1) / 5.f;

        glm::vec3 pos = m_pos + glm::vec3(x,y,z);
        m_particles->pos[m_particles->count + i] = pos;
        m_particles->speed[m_particles->count + i] = glm::vec3(-10,-10,0);
        m_particles->accel[m_particles->count + i] = glm::vec3(0,0,0);
        m_particles->density[m_particles->count + i] = 0.f;
        m_particles->pressure[m_particles->count + i] = 0.f;
        ++internal_counter;
        internal_counter %= 9;
    }
    m_particles->count.fetchAndAddRelaxed(count);
}

void IParticleSimulator::Reset()
{
    m_particles->count.fetchAndStoreRelaxed(0);
}
