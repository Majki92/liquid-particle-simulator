#ifndef SIMULATIONTHREAD_H
#define SIMULATIONTHREAD_H

#include <QThread>
#include <QDebug>

#include <chrono>

#include <simulation/iparticlesimulator.h>

#define MAX_PARTICLES_PER_SEC 1000

enum SimImplType {
    NONE = -1,
    CPP,
    OPENMP,
    CUDA
};

struct ParticleCollection;

class SimulationThread : public QThread
{
    Q_OBJECT
public:
    explicit SimulationThread(ParticleCollection* collection, QThread *parent = 0);

    void run();

    bool IsInProgress() { return m_isInProgress; }
    uint32_t GetSpeed() { return m_particleSpeed; }
    double GetFrameRenderingTime() { return m_frameRenderingTime; }

private:
    volatile bool m_isInProgress;
    bool m_quit;
    uint32_t m_particleSpeed;
    double m_frameRenderingTime;
    SimImplType m_simImplType;
    IParticleSimulator* m_particleSimulators[3];
    std::chrono::time_point<std::chrono::system_clock> m_lastUpdateTime;
public slots:
    void Stop() { m_isInProgress = false; }
    void Start() { m_isInProgress = true; }
    void Terminate() { m_quit = true; }
    void Reset(SimImplType implType);
    void SetSpeed(uint32_t speed) { m_particleSpeed = speed; }
};

#endif // SIMULATIONTHREAD_H
