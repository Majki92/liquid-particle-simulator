
#include "particlesimulatorcuda.h"
#include <stdio.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <cuda.h>
#include <device_functions.h>
#include <cuda_runtime_api.h>
#include <helper_cuda.h>
#include <helper_functions.h>

//////// device helper functions - block start

__device__ float3 operator*(const float3 &a,const float &b) {
return make_float3(a.x*b, a.y*b, a.z*b);
}

__device__ float3 operator/(const float3 &a,const float &b) {
return make_float3(a.x/b, a.y/b, a.z/b);
}

__device__ float3 operator+(const float3 &a,const float3 &b) {
return make_float3(a.x+b.x, a.y+b.y, a.z+b.z);
}

__device__ float3 operator-(const float3 &a,const float3 &b) {
return make_float3(a.x-b.x, a.y-b.y, a.z-b.z);
}

__device__ float dot(const float3 &a, const float3 &b) {
return a.x*b.x + a.y*b.y + a.z*b.z;
}

////////////

__device__
float poly6_dev(float dist2, float s_search_radius) {
    const float coeff = 315.f/(64.f*M_PI*pow(s_search_radius,9));
    const float search_radius2 = s_search_radius*s_search_radius;
    return coeff * pow(search_radius2 - dist2, 3);
}
__device__
float3 poly6Gradient_dev(float3 dist_vec, float dist2, float s_search_radius) {
    const float coeff = -945.f/(32.f*M_PI*pow(s_search_radius,9));
    const float search_radius2 = s_search_radius*s_search_radius;
    return dist_vec * (coeff * (float)pow(search_radius2 - dist2, 2));
}
__device__
float3 spikyGradient_dev(float3 dist_vec, float dist2, float s_search_radius) {
    const float coeff = 15.f/(M_PI*pow(s_search_radius,6));
    float dist = sqrt(dist2);
    return dist_vec * (coeff * (float)pow(s_search_radius - dist,3) / dist);
}
__device__
float poly6Laplacian_dev(float dist2, float s_search_radius) {
    const float coeff = -945.f/(32.f*M_PI*pow(s_search_radius,9));
    const float search_radius2 = s_search_radius*s_search_radius;
    return coeff * (search_radius2 - dist2) * (3.f*search_radius2 - 7.f*dist2);
}
__device__
float viscosityLaplacian_dev(float dist2, float s_search_radius) {
    const float coeff = 45.f/(M_PI*pow(s_search_radius,6));
    return coeff*(s_search_radius - sqrt(dist2));
}
//////// device helper functions - block end


__global__
static void CreateParticles_Kernel(float3 m_emiter_pos, float3* d_pos, float3* d_speed, float3* d_accel, float* d_density, float* d_pressure,
                            int curr_size, int create_count, int internal_counter ) {
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    if( i < create_count) {
        internal_counter = (internal_counter +i) % 9;
        float x = (float)(internal_counter % 3 - 1) / 5.f + m_emiter_pos.x;
        float y = (float)(internal_counter % 3 - 1) / -5.f + m_emiter_pos.y;
        float z = (float)((internal_counter/3) % 3 -1) / 5.f + m_emiter_pos.z;

        // set values
        d_pos[curr_size + i] = make_float3(x,y,z);
        d_speed[curr_size + i] = make_float3(-10,-10,0);
        d_accel[curr_size + i] = make_float3(0,0,0);
        d_density[curr_size + i] = 0.f;
        d_pressure[curr_size + i] = 0.f;
    }
}

uint32_t ParticleSimulatorCuda::CreateParticle(float dt, uint32_t speed, uint32_t count, uint32_t max)
{
    static int internal_counter = 0;
    m_particlesLeft2Create += ((float)speed) * dt;
    uint32_t parts2Create = (uint32_t)m_particlesLeft2Create;
    m_particlesLeft2Create -= parts2Create;

    if (parts2Create + count > max || parts2Create == 0)
            return 0;

    float3 tmp = make_float3(m_pos.x, m_pos.y, m_pos.z);

    int blocks = (parts2Create + m_maxThreads -1) / m_maxThreads;
    dim3 block_dim(m_maxThreads,1,1);
    dim3 grid_dim(blocks,1,1);
    CreateParticles_Kernel<<< grid_dim, block_dim >>>(tmp, sm_particles.d_pos, sm_particles.d_speed, sm_particles.d_accel, sm_particles.d_density, sm_particles.d_pressure,
                                      count, parts2Create, internal_counter);
    checkCudaErrors(cudaGetLastError());
    //device synchronization
    checkCudaErrors(cudaDeviceSynchronize());

    internal_counter += parts2Create;
    internal_counter %= 9;
    return parts2Create;
}

__global__
static void UpdatePosition_Kernel(float dt, uint32_t count, float3* d_pos, float3* d_speed, float3* d_accel)
{
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    if( i < count) {
        float3 n_pos = d_pos[i] + d_speed[i] * dt + d_accel[i] * dt * dt;
        float3 n_speed = (n_pos - d_pos[i])/dt;
        d_speed[i] = n_speed;
        d_pos[i] = n_pos;
    }
}

void ParticleSimulatorCuda::UpdatePosition(float dt, uint32_t count)
{
    if(count == 0) return;

    int blocks = (count + m_maxThreads -1) / m_maxThreads;
    dim3 block_dim(m_maxThreads,1,1);
    dim3 grid_dim(blocks,1,1);

    UpdatePosition_Kernel<<<grid_dim, block_dim>>>(dt, count, sm_particles.d_pos, sm_particles.d_speed, sm_particles.d_accel);
    checkCudaErrors(cudaGetLastError());
    //device synchronization
    checkCudaErrors(cudaDeviceSynchronize());
}


__global__
void ComputeDensityAndPressure_Kernel(uint32_t count, float3* d_pos, float* d_density, float* d_pressure, float s_search_radius, float s_mass, float s_gas_stiffness, float s_rest_density ) {

    int i = threadIdx.x + blockIdx.x * blockDim.x;
    if( i < count) {
        const float search_radius2 = s_search_radius*s_search_radius;
            d_density[i] = 0;

        for(int j=0; j<count; ++j) {
            float3 dist_vec = d_pos[i] - d_pos[j];
            float dist2 = dot(dist_vec,dist_vec);

            if(dist2 <= search_radius2) {
                d_density[i] += poly6_dev(dist2, s_search_radius);
            }
        }

        d_density[i] *= s_mass;
        d_pressure[i] = s_gas_stiffness * (d_density[i] - s_rest_density);
    }
}

void ParticleSimulatorCuda::ComputeDensityAndPressure(uint32_t count) {

    if(count == 0) return;
    int blocks = (count + m_maxThreads -1) / m_maxThreads;
    dim3 block_dim(m_maxThreads,1,1);
    dim3 grid_dim(blocks,1,1);
    ComputeDensityAndPressure_Kernel<<<grid_dim, block_dim>>>(count, sm_particles.d_pos, sm_particles.d_density, sm_particles.d_pressure, s_search_radius, s_mass, s_gas_stiffness, s_rest_density);
    checkCudaErrors(cudaGetLastError());
    checkCudaErrors(cudaDeviceSynchronize());
}
__global__
static void ComputeForces_Kernel(uint32_t count, float3* d_pos, float3* d_speed, float3* d_accel, float* d_density, float* d_pressure,  float s_search_radius, float s_gravity, float s_mass,
                                 float s_surface_treshold, float s_surface_tension, const float3* d_container_pos, const float3* d_container_norm,
                                 float s_viscosity, float s_radius, float s_wall_k, float s_wall_damp) {
    // Numerical stability fix
    s_search_radius = s_search_radius/1.03;
    const float search_radius2 = s_search_radius*s_search_radius;

    int i = threadIdx.x + blockIdx.x * blockDim.x;
    if( i < count) {
        float3 f_pressure = make_float3(0,0,0);
        float3 f_viscosity = make_float3(0,0,0);
        float3 f_surface = make_float3(0,0,0);
        float3 f_gravity = make_float3(0,-s_gravity,0);
        float3 color_field_norm;
        float color_field_lapiacian;

        for(int j=0; j<count; ++j) {
            float3 dist_vec = d_pos[i] - d_pos[j];
            float dist2 = dot(dist_vec,dist_vec);

            if(dist2 <= search_radius2) {
                float3 gradient = poly6Gradient_dev(dist_vec,dist2, s_search_radius);
                float3 spiky_gradient = spikyGradient_dev(dist_vec,dist2, s_search_radius);
                if(i!=j) {
                    //f_pressure += (m_particles->pressure[i]/(float)pow(m_particles->density[i],2)
                                   //+ m_particles->pressure[j]/(float)pow(m_particles->density[j],2)) * spiky_gradient;
                    f_pressure = f_pressure + spiky_gradient * (d_pressure[i] + d_pressure[j]) / (2.0f * d_density[j]);
                    f_viscosity =  f_viscosity + (d_speed[j] - d_speed[i]) * viscosityLaplacian_dev(dist2, s_search_radius) / d_density[j];
                }

                color_field_norm = color_field_norm + gradient / d_density[j];
                color_field_lapiacian = color_field_lapiacian + poly6Laplacian_dev(dist2, s_search_radius) / d_density[j];
            }
        }

        f_pressure = f_pressure * -s_mass;//*m_particles->density[i];
        f_viscosity = f_viscosity * s_viscosity*s_mass;

        color_field_norm = color_field_norm * s_mass;
        color_field_lapiacian = color_field_lapiacian * s_mass;

        float color_field_len = sqrt(dot(color_field_norm,color_field_norm));
        if( color_field_len > s_surface_treshold) {
            f_surface = color_field_norm * (-s_surface_tension  / color_field_len * color_field_lapiacian);
        }

        d_accel[i] = (f_pressure + f_viscosity + f_surface) / d_density[i] + f_gravity;

        for(int k=0; k<5; ++k) {
            float d = dot((d_container_pos[k] - d_pos[i]),d_container_norm[k]) + s_radius;
            if(d > 0.f) {
                d_accel[i] = d_accel[i] + d_container_norm[k] * (s_wall_k * d);
                d_accel[i] = d_accel[i] - d_container_norm[k] * (dot(d_speed[i],d_container_norm[k]) * s_wall_damp) ;
            }
        }
    }
}

void ParticleSimulatorCuda::ComputeForces(uint32_t count) {

    if(count == 0) return;
    int blocks = (count + m_maxThreads -1) / m_maxThreads;
    dim3 block_dim(m_maxThreads,1,1);
    dim3 grid_dim(blocks,1,1);
    ComputeForces_Kernel<<<grid_dim, block_dim>>>(count, sm_particles.d_pos, sm_particles.d_speed, sm_particles.d_accel,
                                    sm_particles.d_density, sm_particles.d_pressure,
                                    s_search_radius, s_gravity, s_mass, s_surface_treshold,
                                    s_surface_tension, sm_particles.d_container_pos, sm_particles.d_container_norm,
                                       s_viscosity, s_radius, s_wall_k, s_wall_damp);
    checkCudaErrors(cudaGetLastError());
    checkCudaErrors(cudaDeviceSynchronize());
}

