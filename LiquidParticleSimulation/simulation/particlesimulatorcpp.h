#ifndef PARTICLESIMULATORCPP_H
#define PARTICLESIMULATORCPP_H

#include <simulation/iparticlesimulator.h>
#include <vector>

//////// device helper functions - block start
float poly6(float dist2);
glm::vec3 poly6Gradient(glm::vec3 dist_vec, float dist2);
glm::vec3 spikyGradient(glm::vec3 dist_vec, float dist2);
float poly6Laplacian(float dist2);
float viscosityLaplacian(float dist2);
//////// device helper functions - block end

class ParticleSimulatorCpp : public IParticleSimulator
{
public:
    ParticleSimulatorCpp(ParticleCollection* collection);

    void Update(float dt, uint32_t speed) override;
private:
    void ComputeDensityAndPressure();
    void ComputeForces();
    void CollisionForce(uint32_t particle_id);
};

#endif // PARTICLESIMULATORCPP_H
