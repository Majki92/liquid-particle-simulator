#ifndef IPARTICLESIMULATOR_H
#define IPARTICLESIMULATOR_H

#include <cstdint>
#include <glm.hpp>

struct ParticleCollection;

struct CollisionInfo {
    bool intersects;
    uint32_t a,b;
    glm::vec3 normal;
    float distance;
};

class IParticleSimulator
{
public:
    IParticleSimulator(ParticleCollection* collection);
    virtual void Update(float dt, uint32_t speed) {}
    virtual void Reset();
protected:
    void AddParticles(uint32_t count);
    ParticleCollection* m_particles;
    float m_particlesLeft2Create;
    glm::vec3 m_pos;
public:
    // particle params
    static const float s_radius;
    static const float s_mass;
    static const float s_search_radius;
    // container params
    static const float s_emiter_height;
    static const float s_wall_damp;
    static const float s_wall_k;
    static const glm::vec3 s_min_bound;
    static const glm::vec3 s_max_bound;
    static const glm::vec3 s_container_pos[5];
    static const glm::vec3 s_container_norm[5];
    // fluid params
    static const float s_gravity;
    static const float s_gas_stiffness;
    static const float s_rest_density;
    static const float s_viscosity;
    static const float s_surface_tension;
    static const float s_surface_treshold;
};

#endif // IPARTICLESIMULATOR_H
