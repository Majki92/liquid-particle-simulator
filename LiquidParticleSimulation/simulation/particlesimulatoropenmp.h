#ifndef PARTICLESIMULATOROPENMP_H
#define PARTICLESIMULATOROPENMP_H

#include <simulation/particlesimulatorcpp.h>
#include <vector>

class ParticleSimulatorOpenMP : public ParticleSimulatorCpp
{
public:
    ParticleSimulatorOpenMP(ParticleCollection* collection);

    void Update(float dt, uint32_t speed) override;
private:
    void ComputeDensityAndPressure();
    void ComputeForces();
    void CollisionForce(uint32_t particle_id);
};

#endif // PARTICLESIMULATOROPENMP_H
