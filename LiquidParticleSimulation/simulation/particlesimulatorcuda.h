#ifndef PARTICLESIMULATORCUDA_H
#define PARTICLESIMULATORCUDA_H

#if defined __cplusplus
extern "C"
#endif
#include "iparticlesimulator.h"
#include <GL/glew.h>
#include <cufft.h>
#include <cuda_gl_interop.h>
#include<cuda_runtime.h>



#include <vector>

#ifndef M_PI
#define M_PI 3.14159265359f
#endif

struct CUDAParticleData {
    float3* speed;
    float3* accel;
    float* density;
    float* pressure;
};

struct CUDAParticleCollection {
    static const int MAX_PARTICLES = 10000;
    // Use buffers below delete code above
    float3* d_pos;
    float3* d_speed;
    float3* d_accel;
    float* d_density;
    float* d_pressure;
    GLint buffer;
    bool registered;

    float3* d_container_pos;
    float3* d_container_norm;
    //CUDAParticleData d_data;
};

class ParticleSimulatorCuda : public IParticleSimulator
{
public:
    ParticleSimulatorCuda(ParticleCollection* collection);
    ~ParticleSimulatorCuda();
    CUDAParticleCollection sm_particles;
    void Update(float dt, uint32_t speed) override;
private:
    void ComputeDensityAndPressure(uint32_t count);
    void ComputeForces(uint32_t count);
    uint32_t CreateParticle(float dt, uint32_t speed, uint32_t count, uint32_t max);
    void UpdatePosition(float dt, uint32_t count);

    int m_maxThreads = 1024;
};

#endif // PARTICLESIMULATORCUDA_H

