#ifndef SHADER_H
#define SHADER_H

#include "utils.h"
#include <string>
#include <fstream>

/* Base shader class - loading shader and use program */
class Shader
{
public:
    Shader(const std::string& filename);
    virtual void SetTestMap(GLuint tex){}
	GLuint mProgram;
    virtual void SetWorldViewProj(const glm::mat4& M){}
    virtual void SetCubeMap(GLuint cubemap){}
    virtual void SetView(const glm::mat4& M){}
    virtual void SetProj(const glm::mat4& M){}
    virtual void SetPointRadius(const float& f){}
    virtual void SetPointScale(const float& f){}
    virtual void SetBackgroundTex(const int& i) {}
    virtual void SetDepthTex(const int& i)     {}
    virtual void SetNormalTex(const int& i)          { }
    virtual void SetThickTex(const int& i) {}
    virtual void SetFar(const float& f)        {}
    virtual void SetNear(const float& f)       {}
    virtual void SetPosTex(const int& i)            {}
    virtual void SetInvProj(const glm::mat4& M)     {}

};

/* Class for rendering sky*/
class SkyShader : public Shader
{
public:
    SkyShader(const std::string& filename);
    ~SkyShader(){}

    //Set specific uniforms
    void SetWorldViewProj(const glm::mat4& M)           {  GL_CHECK(glUniformMatrix4fv(MVPloc, 1, GL_FALSE, glm::value_ptr(M))); }
	void SetCubeMap(GLuint cubemap);

private:
    // Uniforms location in program
	GLint MVPloc;
	GLint cubeMapLoc;
};

class ParticleShader : public Shader
{
    public:
        ParticleShader(const std::string& filename);
        ~ParticleShader(){}
        void SetView(const glm::mat4& M)           {  GL_CHECK(glUniformMatrix4fv(Vloc, 1, GL_FALSE, glm::value_ptr(M))); }
        void SetProj(const glm::mat4& M)           {  GL_CHECK(glUniformMatrix4fv(Ploc, 1, GL_FALSE, glm::value_ptr(M))); }
        void SetPointRadius(const float& f)        {  GL_CHECK(glUniform1f(PRloc, f)); }
        void SetPointScale(const float& f)         {  GL_CHECK(glUniform1f(PSloc, f)); }
    private:
        GLint Vloc;
        GLint Ploc;
        GLint PRloc;
        GLint PSloc;
};

class ThickShader : public Shader
{
    public:
        ThickShader(const std::string& filename);
        ~ThickShader(){}
        void SetView(const glm::mat4& M)           {  GL_CHECK(glUniformMatrix4fv(Vloc, 1, GL_FALSE, glm::value_ptr(M))); }
        void SetProj(const glm::mat4& M)           {  GL_CHECK(glUniformMatrix4fv(Ploc, 1, GL_FALSE, glm::value_ptr(M))); }
        void SetPointRadius(const float& f)        {  GL_CHECK(glUniform1f(PRloc, f)); }
        void SetPointScale(const float& f)         {  GL_CHECK(glUniform1f(PSloc, f)); }
    private:
        GLint Vloc;
        GLint Ploc;
        GLint PRloc;
        GLint PSloc;
};

class BlurShader : public Shader
{
    public:
        BlurShader(const std::string& filename);
        ~BlurShader(){}
        void SetDepthTex(const int& i)     {  GL_CHECK(glUniform1i(Depthloc, i)); }
        void SetFar(const float& f)        {  GL_CHECK(glUniform1f(Floc, f)); }
        void SetNear(const float& f)       {  GL_CHECK(glUniform1f(Nloc, f)); }
    private:
        GLint Depthloc;
        GLint Floc;
        GLint Nloc;
};

class NormalShader : public Shader
{
    public:
        NormalShader(const std::string& filename);
        ~NormalShader(){}
        void SetDepthTex(const int& i)          {  GL_CHECK(glUniform1i(Depthloc, i)); }
        void SetPosTex(const int& i)            {  GL_CHECK(glUniform1i(Posloc, i)); }
        void SetInvProj(const glm::mat4& M)     {  GL_CHECK(glUniformMatrix4fv(IPloc, 1, GL_FALSE, glm::value_ptr(M))); }
        void SetFar(const float& f)             {  GL_CHECK(glUniform1f(Floc, f)); }
        void SetNear(const float& f)            {  GL_CHECK(glUniform1f(Nloc, f)); }
    private:
        GLint IPloc;
        GLint Depthloc;
        GLint Posloc;
        GLint Floc;
        GLint Nloc;
};

class ResolveShader : public Shader
{
    public:
        ResolveShader(const std::string& filename);
        ~ResolveShader(){}
        void SetBackgroundTex(const int& i)         {  GL_CHECK(glUniform1i(Backgroundloc, i)); }
        void SetView(const glm::mat4& M)           {  GL_CHECK(glUniformMatrix4fv(Vloc, 1, GL_FALSE, glm::value_ptr(M))); }
        void SetInvProj(const glm::mat4& M)     {  GL_CHECK(glUniformMatrix4fv(IPloc, 1, GL_FALSE, glm::value_ptr(M))); }
        void SetDepthTex(const int& i)          {  GL_CHECK(glUniform1i(Depthloc, i)); }
        void SetPosTex(const int& i)            {  GL_CHECK(glUniform1i(Posloc, i)); }
        void SetNormalTex(const int& i)          {  GL_CHECK(glUniform1i(NormalLoc, i)); }
        void SetThickTex(const int& i)            {  GL_CHECK(glUniform1i(Thickloc, i)); }
        void SetFar(const float& f)             {  GL_CHECK(glUniform1f(Floc, f)); }
        void SetNear(const float& f)            {  GL_CHECK(glUniform1f(Nloc, f)); }
        void SetCubeMap(GLuint cubemap);
    private:
        GLint Backgroundloc;
        GLint Vloc;
        GLint IPloc;
        GLint Depthloc;
        GLint NormalLoc;
        GLint Posloc;
        GLint Thickloc;
        GLint cubeMapLoc;
        GLint Floc;
        GLint Nloc;
};

/* Test shader */
class TestShader : public Shader
{
public:
    TestShader(const std::string& filename);
    ~TestShader(){}
	void SetTestMap(GLuint tex);
    //Set specific uniforms
    void SetWorldViewProj(const glm::mat4& M)           {  GL_CHECK(glUniformMatrix4fv(MVPloc, 1, GL_FALSE, glm::value_ptr(M))); }
private:

    GLint MVPloc;
	GLint testMapLoc;
};

#endif
