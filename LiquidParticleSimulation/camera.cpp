#include "camera.h"
#include <QDebug>

/* Initialize camera base vectors, position and frustrum */
Camera::Camera() : mPosition(0.0f, 15.0f, 17.0f),
                     mRight(1.0f, 0.0f, 0.0f),
                     mUp(0.0f, 1.0f, 0.0f),
                     mLook(0.0f, -2.0f, -2.0f)
{
    SetLens(glm::quarter_pi<float>(), 1.7778f, 1.0f, 1000.0f);
}

Camera::~Camera()
{
}

glm::vec3 Camera::GetPosition() const
{
    return mPosition;
}

void Camera::SetPosition(float x, float y, float z)
{
    mPosition = glm::vec3(x, y, z);
}

void Camera::SetPosition(const glm::vec3& v)
{
    mPosition = v;
}

glm::vec3 Camera::GetRight() const
{
    return mRight;
}

glm::vec3 Camera::GetUp() const
{
    return mUp;
}

glm::vec3 Camera::GetLook() const
{
    return mLook;
}

void Camera::SetLens(float fovY, float aspect, float zn, float zf)
{
    /* Cache properties */
    mFovY = fovY;
    mAspect = aspect;
    mNearZ = zn;
    mFarZ = zf;

    mNearWindowHeight = 2.0f * mNearZ * tanf(0.5f*mFovY);
    mFarWindowHeight = 2.0f * mFarZ * tanf(0.5f*mFovY);

    mProj = glm::perspective(mFovY, mAspect, mNearZ, mFarZ);
}

void Camera::LookAt(const glm::vec3& pos, const glm::vec3& target, const glm::vec3& up)
{
    mPosition = pos;
    mLook = glm::normalize(target - pos);
    mRight = glm::normalize(glm::cross(mLook, mPosition));
    mUp = glm::cross(mLook, mRight);
}

glm::mat4 Camera::View() const
{
    return mView;
}

glm::mat4 Camera::Proj() const
{
    return mProj;
}

glm::mat4 Camera::InvProj()
{
    return glm::inverse(mProj);
}

glm::mat4 Camera::ViewProj() const
{
    return mProj * mView;
}

void Camera::Strafe(float d)
{
    mPosition += d * mRight;
}

void Camera::Walk(float d)
{
    mPosition += d * mLook;
}

void Camera::Pitch(float angle)
{
    /* Rotate up and look vector about the right vector */
    glm::mat4 r = glm::rotate(glm::mat4(1.0f), angle, mRight);
    glm::mat3 nr = glm::inverseTranspose(glm::mat3(r));
    mUp =  mUp * nr;
    mLook =  mLook * nr;
}

void Camera::RotateY(float angle)
{
    /* Rotate the basis vectors about the world y-axis */
    glm::mat4 r = glm::rotate(glm::mat4(1.0f), angle, glm::vec3(0.0f, 1.0f, 0.0f));
    glm::mat3 nr = glm::inverseTranspose(glm::mat3(r));
    mUp =  mUp * nr;
    mLook = mLook * nr;
    mRight = mRight * nr;
}

void Camera::UpdateViewMatrix()
{
    /* Build up new view matrix */
    mLook = glm::normalize(mLook);
    mUp = glm::normalize(glm::cross(mRight, mLook));
    mRight = glm::cross(mLook, mUp );
    mView = glm::lookAt(mPosition, mLook + mPosition, mUp);
}
